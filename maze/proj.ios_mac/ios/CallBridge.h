//
//  CallBridge.h
//  flappybird
//
//  Created by Le Thanh Hai on 3/30/14.
//
//

#ifndef __flappybird__CallBridge__
#define __flappybird__CallBridge__

#include <iostream>
#include "cocos2d.h"

class CallBridge {
    
public:
    static void callShowLeaderboard();
    static void callShareScoreToFacebook(int score);
    static void callShareScoreToGameCenter(int score, int type);
    
    static void callAppRate();
        
    static void saveBestScore(int score);
    
    static void loadInterstitial();
    static void showInterstitial();
    
    static void bringAdsToTop();
    static void bringAdsToBottom();
    
    static void moreGame();
    static void shareFacebook(int score);
    static void shareFacebookPicture(const char* str);
    static void isShowAds(bool flag);
    static void buyItem(int index);
    static void getListIAPItem(int index);
    static void restonePurchase();
    static void firstShareFacebook();
};
#endif /* defined(__flappybird__CallBridge__) */
