#import <UIKit/UIKit.h>
#import "GameCenterManager.h"
#import <GameKit/GameKit.h>
#import <StartApp/StartApp.h>
#import <Social/Social.h>
#import "IAPHelper.h"
#import "MyIAPHelper.h"
#import "MBProgressHUD.h"

@class RootViewController;

@interface AppController : NSObject <UIApplicationDelegate,GKLeaderboardViewControllerDelegate,GameCenterManagerDelegate, STABannerDelegateProtocol> {
    UIWindow *window;
    BOOL loadedAds;
    STAStartAppAd* startAppAd;
    STABannerView* bannerView;
    BOOL bannerLocation;
    BOOL addBannerToView;
    NSMutableArray *lstProducts;
    UIActivityIndicatorView *indicator;
    UIView * subView;
}

@property(nonatomic, readonly) RootViewController* viewController;
@property(nonatomic, strong) GameCenterManager *gameCenterManager;
- (void)showLeaderboard;
- (void)rateApp;
- (void)saveBestScore:(int)best_score;
- (void)loadInterstitial;
- (void)showInterstitial;
- (void) bringAdsToTop;
- (void) bringAdsToBottom;
- (void) moreGame;
- (void)shareFacebook:(int)best_score;
- (void) firstShareFacebook;
- (void) isShowAds: (bool) flag;
- (void) buyItem: (int) index;
- (void) transasionSuccess: (NSString *)productIdentifier;
- (void) transasionFail;
- (void) getListIAPItem: (int) index;
- (void) shareFacebookPicture: (NSString *) path;
- (void) restonePurchase;
- (void) nothingRestone;
@end

