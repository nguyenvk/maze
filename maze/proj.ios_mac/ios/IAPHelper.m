
#import "IAPHelper.h"
#import "AppController.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperProductPurchasedFailNotification = @"IAPHelperProductPurchasedFailNotification";

@interface IAPHelper()<SKProductsRequestDelegate,SKPaymentTransactionObserver>
{
    SKProductsRequest *_productRequest;
    RequestProductsCompletionHandler _completionHandler;
    NSSet *_productIdentifiers;
    NSMutableSet *_purchasedProductIdentifiers;
}
@end
@implementation IAPHelper
-(id)initWithProductIdentifiers:(NSSet *)productIdentifiers{
    if (self = [super init]) {
        //store product identifiers
        _productIdentifiers = productIdentifiers;
        
        //check for previously purchased product
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString *productIdentifier in _productIdentifiers) {
            BOOL isProductPurchased = [[NSUserDefaults standardUserDefaults]boolForKey:productIdentifier];
            if (isProductPurchased) {
                NSLog(@"Previously purchased: %@", productIdentifier);
                [_purchasedProductIdentifiers addObject:productIdentifier];
            }else{
                NSLog(@"Not purchase: %@",productIdentifier);
            }
        }
        [_purchasedProductIdentifiers retain];
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    return self;
}
-(void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler{
    _completionHandler = [completionHandler copy];
    
    _productRequest = [[SKProductsRequest alloc]initWithProductIdentifiers:_productIdentifiers];
    _productRequest.delegate = self;
    [_productRequest start];
}
#pragma mark - 
-(void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response
{
    _productRequest = nil;
    NSArray *products = response.products;
    for (SKProduct *product in products) {
        NSLog(@"Found product : %@, %@, %f",product.productIdentifier,product.localizedTitle,product.price.floatValue);
        
    }
    _completionHandler(YES,products);
    _completionHandler = nil;
}
-(void)request:(SKRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"Failed : %@",error);
    _productRequest = nil;
    _completionHandler(NO,nil);
    _completionHandler = nil;
}
#pragma mark -
- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);
    
    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
            {
                if (transaction.downloads) {
                    [[SKPaymentQueue defaultQueue]
                     startDownloads:transaction.downloads];
                }else{
                    [self completeTransaction:transaction];
                }
            }
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
            {
                NSLog(@"Restore download begin");
                if (transaction.downloads) {
                    [[SKPaymentQueue defaultQueue]
                     startDownloads:transaction.downloads];
                }else{
                    [self restoreTransaction:transaction];
                }
            }
            default:
                break;
        }
    };
}
//- (void)paymentQueue:(SKPaymentQueue *)queue removedTransactions:(NSArray *)transactions
//{
//    NSLog(@"Purchase removedTransactions");
//    
//    // Release the transaction observer since transaction is finished/removed.
//    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
//}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    NSLog(@"transaction = %@",[transaction description]);
    NSLog(@"trasction date = %@", transaction.transactionDate);
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    AppController *appc = (AppController*)[UIApplication sharedApplication].delegate;
    [appc transasionSuccess: transaction.payment.productIdentifier];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction... %@",transaction);
    if (transaction.originalTransaction.payment.productIdentifier) {
        [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
        [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    }else{
        NSLog(@"NILLLLLLLL");
    }
   
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedFailNotification object:transaction userInfo:nil];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[transaction.error localizedDescription] delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    [alert show];
    
    if (transaction.error.code != SKErrorPaymentCancelled) {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    AppController *appc = (AppController*)[UIApplication sharedApplication].delegate;
    [appc transasionFail];
}

// Add new method
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    NSLog(@"product Identifier = %@",productIdentifier);
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
    
}
- (void)restoreCompletedTransactions {
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}
//Then this delegate Function Will be fired
- (void) paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue
{
    //purchasedItemIDs = [[NSMutableArray alloc] init];
    
    NSLog(@"received restored transactions: %i", queue.transactions.count);
    AppController *appc = (AppController*)[UIApplication sharedApplication].delegate;
    if (queue.transactions.count == 0) {
        [appc nothingRestone];
    } else {
        for (SKPaymentTransaction *transaction in queue.transactions)
        {
            NSString *productID = transaction.payment.productIdentifier;
            [appc transasionSuccess: productID];
        }
    }
    
}
@end
