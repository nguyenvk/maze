//
//  CallBridge.cpp
//  flappybird
//
//  Created by Le Thanh Hai on 3/30/14.
//
//

#include "CallBridge.h"
#include "AppController.h"
//#import "Appirater.h"

using namespace cocos2d;
void CallBridge::saveBestScore(int best_score)
{
    AppController *_appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    [_appDelegate saveBestScore:best_score];
}
void CallBridge::callShareScoreToGameCenter(int score, int type)
{
    AppController *_appDelegate = (AppController *)[[UIApplication sharedApplication] delegate];
    if (type == 1) {
        [_appDelegate.gameCenterManager reportScore:score forCategory:kLeaderboardEasy];
    } else if (type == 2) {
        [_appDelegate.gameCenterManager reportScore:score forCategory:kLeaderboardNormal];
    } else if (type == 3) {
        [_appDelegate.gameCenterManager reportScore:score forCategory:kLeaderboardHard];
    } else {
        [_appDelegate.gameCenterManager reportScore:score forCategory:kLeaderboardInsane];
    }
}
void CallBridge::callShowLeaderboard(){
    
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
        [_appController showLeaderboard];
    }
}

void CallBridge::callShareScoreToFacebook(int score)
{
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
    }
}
void CallBridge::shareFacebookPicture(const char* str) {
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
        [_appController shareFacebookPicture: [NSString stringWithUTF8String:str]];
    }
}
void CallBridge::callAppRate()
{
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
        [_appController rateApp];
    }
}
void CallBridge::bringAdsToTop()
{
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
        [_appController bringAdsToTop];
    }
}void CallBridge::bringAdsToBottom()
{
    id _appController = [[UIApplication sharedApplication] delegate];
    if ([_appController isKindOfClass:[AppController class]]) {
        [_appController bringAdsToBottom];
    }
}
void CallBridge::loadInterstitial()
{
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController loadInterstitial];
}

void CallBridge::showInterstitial()
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isHideAds"] == false) {
        AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
        [appController showInterstitial];
    }
}
void CallBridge::moreGame()
{
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController moreGame];
}
void CallBridge::shareFacebook(int score)
{
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController shareFacebook:score];
}
void CallBridge::firstShareFacebook() {
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController firstShareFacebook];
}
void CallBridge::isShowAds(bool flag) {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isHideAds"] == false) {
        AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
        [appController isShowAds:flag];
    }
}
void CallBridge::buyItem(int index) {
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController buyItem: index];
}
void CallBridge::restonePurchase(){
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController restonePurchase];
}
void CallBridge::getListIAPItem(int index) {
    AppController *appController = (AppController*)[UIApplication sharedApplication].delegate;
    [appController getListIAPItem: index];
}