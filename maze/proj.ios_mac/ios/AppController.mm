/****************************************************************************
 Copyright (c) 2010 cocos2d-x.org
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#import "AppController.h"
#import "platform/ios/CCEAGLView-ios.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"
#include "ACPReminder.h"

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

- (void)saveBestScore:(int)best_score
{
    NSUserDefaults *userdefault = [NSUserDefaults standardUserDefaults];
    NSString *strScore = [NSString stringWithFormat:@"%d",best_score];
    [userdefault setObject:strScore forKey:@"BEST_SCORE"];
    [userdefault synchronize];
}
- (void)rateApp
{
    NSLog(@"rate");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kAppUrl]];
}
-(void)showLeaderboard
{
    NSLog(@"show leader board");
    GKLeaderboardViewController *leaderboard = [[GKLeaderboardViewController alloc] init];
    if (leaderboard!=NULL) {
        leaderboard.category = nil;
        leaderboard.timeScope = GKLeaderboardTimeScopeAllTime;
        leaderboard.leaderboardDelegate = self;
        [self.viewController presentViewController:leaderboard animated:YES completion:nil];
    }
}
-(void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginGameCenter
{
    if ([GameCenterManager isGameCenterAvailable]) {
        self.gameCenterManager = [[GameCenterManager alloc] init];
        self.gameCenterManager.delegate = self;
        [self.gameCenterManager authenticateLocalUser];
    }
    else{
        [[[UIAlertView alloc]initWithTitle:@"Message" message:@"Not support GameCenter" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil] show];
    }
}

- (void)displayBanner{
    startAppAd = [[STAStartAppAd alloc] init];
    addBannerToView = false;
    [startAppAd loadAd];
    if (bannerView == nil) {
        bannerView = [[STABannerView alloc] initWithSize:STA_AutoAdSize autoOrigin:STAAdOrigin_Bottom
                                                withView:self.viewController.view withDelegate:self];
        [self.viewController.view addSubview:bannerView];
    }
}
- (void) failedLoadBannerAd:(STABannerView*)banner withError:(NSError *)error {
    [bannerView hideBanner];
}
- (void) didDisplayBannerAd:(STABannerView*)banner {
//    if (bannerLocation) {
//        CGRect adsFrame = bannerView.frame;
//        [bannerView setFrame:CGRectMake(0, 0, adsFrame.size.width, adsFrame.size.height)];
//        [bannerView setAutoresizingMask:STAAdOrigin_Top];
//    } else {
//        CGRect adsFrame = bannerView.frame;
//        [bannerView setFrame:CGRectMake(0, [[UIScreen mainScreen]applicationFrame].size.height - 50, adsFrame.size.width, adsFrame.size.height)];
//        //bannerView_.frame.origin = CGPointMake(0, [[UIScreen mainScreen]applicationFrame].size.height - 30);
//        [bannerView setAutoresizingMask:STAAdOrigin_Bottom];
//    }
}
- (void) bringAdsToTop {
    bannerLocation = true;
    CGRect adsFrame = bannerView.frame;
    [bannerView setFrame:CGRectMake(0, 0, adsFrame.size.width, adsFrame.size.height)];
    [bannerView setAutoresizingMask:STAAdOrigin_Top];
}
- (void) bringAdsToBottom {
    bannerLocation = false;
    CGRect adsFrame = bannerView.frame;
    [bannerView setFrame:CGRectMake(0, [[UIScreen mainScreen]applicationFrame].size.height - 50, adsFrame.size.width, adsFrame.size.height)];
    //bannerView_.frame.origin = CGPointMake(0, [[UIScreen mainScreen]applicationFrame].size.height - 30);
    [bannerView setAutoresizingMask:STAAdOrigin_Bottom];
}
- (void)showInterstitial
{
    [bannerView showBanner];
    [startAppAd showAd];
    [startAppAd loadAd];
}
- (void)moreGame
{
    NSString *iTunesLink = @"itms-apps://itunes.apple.com/us/artist/vo-kim-nguyen/id910780425";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
}

- (void) shareFacebook:(int)best_score {
    SLComposeViewController *fControler = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [fControler setInitialText:[NSString stringWithFormat:@"I got %d score on this amazing game, let 's play with me!", best_score]];
    [fControler addURL:[NSURL URLWithString:kAppUrl]];
    [_viewController presentViewController:fControler animated:YES completion:nil];
}
- (void) shareFacebookPicture:(NSString *) str {
    SLComposeViewController *fControler = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [fControler setInitialText:[NSString stringWithFormat:@"I sloved this amazing Maze!!!"]];
    [fControler addURL:[NSURL URLWithString:kAppUrl]];
    UIImage *uim = [UIImage imageNamed:str];
    [fControler addImage:uim];
    [_viewController presentViewController:fControler animated:YES completion:nil];
}
- (void) firstShareFacebook {
    NSUserDefaults *userdefaults = [NSUserDefaults standardUserDefaults];
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller setInitialText:@"I found this amazing Maze game, let 's play with me !!!!"];
    [controller addURL:[NSURL URLWithString:kAppUrl]];
    [_viewController presentViewController:controller animated:YES completion:^{
        if (![userdefaults boolForKey:@"first_share_facebook"]) {
            [userdefaults setBool:true forKey:@"first_share_facebook"];
            NSInteger numBall = [userdefaults integerForKey:@"numBall"];
            [userdefaults setInteger:numBall + 500 forKey:@"numBall"];
            [userdefaults synchronize];
        }
    }];

}
- (void) isShowAds: (bool) flag {
    if (flag) {
        [bannerView showBanner];
    } else {
        [bannerView hideBanner];
    }
}
// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // Override point for customization after application launch.
    // iOS 8 support
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]){
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Init the CCEAGLView
    CCEAGLView *eaglView = [CCEAGLView viewWithFrame: [window bounds]
                                         pixelFormat: kEAGLColorFormatRGBA8
                                         depthFormat: GL_DEPTH24_STENCIL8_OES
                                  preserveBackbuffer: NO
                                          sharegroup: nil
                                       multiSampling: NO
                                     numberOfSamples: 0];
    
    //[eaglView setMultipleTouchEnabled:YES];
    
    // Use RootViewController manage CCEAGLView
    _viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    _viewController.wantsFullScreenLayout = YES;
    _viewController.view = eaglView;
    
    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: _viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:_viewController];
    }
    
    // initialize the SDK with your appID and devID
    STAStartAppSDK* sdk = [STAStartAppSDK sharedInstance];
    sdk.appID = startAppId;
    sdk.devID = startAppDeveloperId;
    
    bool isHideAds = [[NSUserDefaults standardUserDefaults] boolForKey:@"isHideAds"];
    if (!isHideAds) {
        [self displayBanner];
    }
    [self loginGameCenter];
    bannerLocation = true;
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:true];
    
    // IMPORTANT: Setting the GLView should be done after creating the RootViewController
    cocos2d::GLView *glview = cocos2d::GLViewImpl::createWithEAGLView(eaglView);
    cocos2d::Director::getInstance()->setOpenGLView(glview);
    
    cocos2d::Application::getInstance()->run();
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    cocos2d::Director::getInstance()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    //We don't need to call this method any more. It will interupt user defined game pause&resume logic
    cocos2d::Director::getInstance()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    ACPReminder * localNotifications = [ACPReminder sharedManager];
    
    //Settings
    localNotifications.messages = @[@"Hey, we miss you! Come back, select Insane Mode and slove a Maze"];
    localNotifications.timePeriods = @[@(1),@(2),@(3),@(4),@(5),@(6),@(7),@(8),@(9),@(10)]; //days
    localNotifications.appDomain = kAppBundleId;
    localNotifications.randomMessage = NO; //By default is NO (optional)
    localNotifications.testFlagInSeconds = NO; //By default is NO (optional) -->For testing purpose only!
    localNotifications.circularTimePeriod = NO; // By default is NO (optional)
    
    [localNotifications createLocalNotification];

    cocos2d::Application::getInstance()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::Application::getInstance()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}
//inapp puchase
- (void) getListIAPItem: (int) index {
    NSLog(@"Start get list item");
    [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
    appDelegate->showMenuDump();
    [[MyIAPHelper shareInstance]requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            lstProducts = [NSMutableArray arrayWithArray:products];
            [lstProducts retain];
            //[MBProgressHUD hideAllHUDsForView:self.viewController.view animated:YES];
            [[MyIAPHelper shareInstance] buyProduct:[lstProducts objectAtIndex:index]];
        } else {
            [MBProgressHUD hideAllHUDsForView:self.viewController.view animated:YES];
            AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
            appDelegate->hideMenuDump();
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Connection Error"
                                                             message:@"Can not connect to Itune"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
            //[alert addButtonWithTitle:@"GOO"];
            [alert show];

        }
    }];
}
- (void) buyItem: (int) index {
    NSLog(@"Start mua");
    [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    SKProduct *skp = [lstProducts objectAtIndex:index];
    [[MyIAPHelper shareInstance] buyProduct:skp];
}
- (void) restonePurchase {
    AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
    appDelegate->showMenuDump();
    [MBProgressHUD showHUDAddedTo:self.viewController.view animated:YES];
    [[MyIAPHelper shareInstance] restoreCompletedTransactions];
}
- (void) nothingRestone {
    [MBProgressHUD hideAllHUDsForView:self.viewController.view animated:YES];
    AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
    appDelegate->hideMenuDump();
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Hmmm"
                                                     message:@"Nothing to restone!"
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles: nil];
    //[alert addButtonWithTitle:@"GOO"];
    [alert show];
}
- (void)transasionSuccess: (NSString *)productIdentifier {
    [MBProgressHUD hideAllHUDsForView:self.viewController.view animated:YES];
    int numHat = [[NSUserDefaults standardUserDefaults] integerForKey:@"santaHat"];
    AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
    appDelegate->hideMenuDump();
    
    if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.removeAds"]) {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isHideAds" ];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self isShowAds: false];
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.unlockHard"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"hardFlag"];
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"selectLevel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->finishPurchase();
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.unlockInsane"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"insaneFlag"];
        [[NSUserDefaults standardUserDefaults] setInteger:4 forKey:@"selectLevel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->finishPurchase();
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.200hat"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:(numHat + 200) forKey:@"santaHat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->updateSantaHat();
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.1100hat"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:(numHat + 1100) forKey:@"santaHat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->updateSantaHat();
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.2300hat"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:(numHat + 2300) forKey:@"santaHat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->updateSantaHat();
    } else if ([productIdentifier isEqualToString:@"com.nguyenhaistudio.mazeClassic.4700hat"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:(numHat + 4700) forKey:@"santaHat"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate->updateSantaHat();
    }
}
- (void)transasionFail {
    [MBProgressHUD hideAllHUDsForView:self.viewController.view animated:YES];
    AppDelegate *appDelegate = dynamic_cast<AppDelegate*>(cocos2d::Application::getInstance());
    appDelegate->hideMenuDump();
}

- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
