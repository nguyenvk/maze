
#import "MyIAPHelper.h"

@implementation MyIAPHelper
+ (MyIAPHelper *)shareInstance {
    static dispatch_once_t once;
    static MyIAPHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSSet * productIdentifiers = [NSSet setWithObjects:
                                      @"com.nguyenhaistudio.mazeClassic.removeAds",
                                      @"com.nguyenhaistudio.mazeClassic.unlockInsane",
                                      @"com.nguyenhaistudio.mazeClassic.unlockHard",
                                      @"com.nguyenhaistudio.mazeClassic.200hat",
                                      @"com.nguyenhaistudio.mazeClassic.1100hat",
                                      @"com.nguyenhaistudio.mazeClassic.2300hat",
                                      @"com.nguyenhaistudio.mazeClassic.4700hat",
                                      nil];

        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}
@end
