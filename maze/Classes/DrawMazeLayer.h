//
//  DrawMazeLayer.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/8/14.
//
//

#ifndef __maze__DrawMazeLayer__
#define __maze__DrawMazeLayer__

#include <stdio.h>
#include "Model.h"
#include "PlayerSprite.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
class DrawMazeLayerProtocol {
public:
    virtual void winGame() = 0;
    virtual void showWinLayer() = 0;
};

class DrawMazeLayer: public LayerColor {
public:
    virtual bool init(__Array *maze, int numRow, int numCol);
    static DrawMazeLayer* create(__Array *maze, int numRow, int numCol);
    __Array *_maze, *_resultPath;
    //void update(float time);
    void drawSloveMaze(float dt);
    void setMazeCampain(Point startPoint, Point endPoint);
    Size _winSize;
    int _numRow, _numCol, _indexSloveDraw = 0,_indexAutoMove = 0;
    float cellWidth, cellHeight;
    void autoMove(float dt);
    DrawMazeLayerProtocol *delegate;
    void runDebug(__Array *maze, __Dictionary *startPoint, __Dictionary *endPoint, int numRow, int numCol);
    void addReindder();
    void addWinReindder();
    int _timeHint = 0;
    bool drawHint();
    void addSantaHat();
    Vector<__Array*> _santahatLoc;
    __Array *_santahatArrSpr;
};
#endif /* defined(__maze__DrawMazeLayer__) */
