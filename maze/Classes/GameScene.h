//
//  GameScene.h
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/19/14.
//
//

#ifndef __SwingCopter__GameScene__
#define __SwingCopter__GameScene__

#include <iostream>
#include "cocos2d.h"
#include "GameLayer.h"
#include "Model.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;


class GameScene : public Scene
{
public:
	GameScene();
    
	~GameScene();
    
	bool virtual init(int numRow, int numCol);
    
    void restart();
    static GameScene* create(int numRow, int numCol);
	//CREATE_FUNC(GameScene);
};
#endif /* defined(__SwingCopter__GameScene__) */
