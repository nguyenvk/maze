//
//  SelectLevelLayer.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/11/14.
//
//

#include "SelectLevelLayer.h"

USING_NS_CC;

SelectLevelLayer* SelectLevelLayer::create()
{
    SelectLevelLayer *pRet = new SelectLevelLayer();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool SelectLevelLayer::init()
{
    _winSize = Director::getInstance()->getVisibleSize();
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0), _winSize.width, _winSize.height))
    {
        return false;
    }
    MenuItemSprite *mnSP = MenuItemSprite::create(Sprite::create("blank.png", Rect(0, 0, 640, 1024)), NULL);
    mnSP->setPosition(_winSize.width/2, _winSize.height/2);
    mnSP->setOpacity(0);
    Menu *mn = Menu::create(mnSP, NULL);
    mn->setPosition(0,100);
    this->addChild(mn);
    
    _bg = Sprite::create("bgSelectLevel.png");
    
    _bg->setAnchorPoint(Point::ANCHOR_TOP_LEFT);
    _bg->setPosition(_winSize.width / 2 - _bg->getContentSize().width / 2, _winSize.height / 2 + _bg->getContentSize().height / 2);
    this->addChild(_bg);
    _bg->setOpacity(0);
    _bg->setScale(0.01);
    this->setVisible(false);
    return true;
}
void SelectLevelLayer::aniShow() {
    this->setVisible(true);
    _bg->removeAllChildrenWithCleanup(true);
    _bg->runAction(FadeIn::create(0.3));
    _bg->runAction(Sequence::create(EaseOut::create(ScaleTo::create(0.3, 1), 2), DelayTime::create(0.3), CallFunc::create([&] {
        this->createMenu();
    }), NULL));
}
void SelectLevelLayer::aniHide(Ref *sender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    _bg->runAction(Sequence::create(EaseIn::create(ScaleTo::create(0.3, 0.01), 2), CallFunc::create([&] {
        _bg->setOpacity(0);
        this->setVisible(false);
    }), NULL));
}
void SelectLevelLayer::createMenu() {
    Size bgSize = _bg->getContentSize();
    Label *lb = Label::createWithSystemFont("CLOSE", "Roboto-Light", 36);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *close = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::aniHide, this));
    close->setPosition(bgSize.width /2, bgSize.height * 0.8 / 6);
    
    lb = Label::createWithSystemFont("EASY", "Roboto-Light", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *easeMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::changeLevel, this));
    easeMenu->setPosition(bgSize.width /2, bgSize.height * 5 / 6);
    easeMenu->setTag(1);
    
    lb = Label::createWithSystemFont("NORMAL", "Roboto-Light", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *normalMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::changeLevel, this));
    normalMenu->setPosition(bgSize.width /2, bgSize.height * 4 / 6);
    normalMenu->setTag(2);
    
    lb = Label::createWithSystemFont("HARD", "Roboto-Light", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *hardMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::changeLevel, this));
    hardMenu->setPosition(bgSize.width /2, bgSize.height * 3 / 6);
    hardMenu->setTag(3);
    
    lb = Label::createWithSystemFont("INSANE", "Roboto-Light", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *insaneMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::changeLevel, this));
    insaneMenu->setPosition(bgSize.width /2, bgSize.height * 2 / 6);
    insaneMenu->setTag(4);
    
    Menu *menu = Menu::create(close, easeMenu, normalMenu, hardMenu, insaneMenu, NULL);
    menu->setPosition(0,0);
    menu->setOpacity(0);
    _bg->addChild(menu);
    menu->runAction(FadeIn::create(0.3));
}
void SelectLevelLayer::changeLevel(cocos2d::Ref *sender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    MenuItemLabel *mnSend = static_cast<MenuItemLabel*>(sender);
    if (mnSend->getTag() == 1) {
        UserDefault::getInstance()->setIntegerForKey(selectLevel, 1);
        this->aniHide(sender);
    } else if (mnSend->getTag() == 2) {
        if (UserDefault::getInstance()->getIntegerForKey(normalFlag, 0)) {
            UserDefault::getInstance()->setIntegerForKey(selectLevel, 2);
            this->aniHide(sender);
        } else {
            MenuItemSprite *dumpMenu = MenuItemSprite::create(Sprite::create("noteLevel.png"), NULL);
            dumpMenu->setPosition(_bg->getContentSize().width /2,  _bg->getContentSize().height * 2 );
            Menu *menu = Menu::create(dumpMenu, NULL);
            menu->setPosition(0,0);
            menu->setName("dumpMenu");
            _bg->addChild(menu,0,1);
            dumpMenu->runAction( Sequence::create(EaseBackOut::create(MoveTo::create(0.3, Point(_bg->getContentSize().width / 2, _bg->getContentSize().height / 2))), DelayTime::create(0.2),CallFunc::create([&] {
                Label *lb = Label::createWithSystemFont("PLEASE COMPLETE EASY MODE FIRST!", "Roboto-Thin", 24);
                lb->setPosition(_bg->getContentSize().width /2, _bg->getContentSize().height * 8.2 / 16);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                _bg->getChildByName("dumpMenu")->addChild(MenuItemLabel::create(lb));
                lb->runAction(FadeIn::create(0.3));
                lb = Label::createWithSystemFont("OK", "Roboto-Thin", 36);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                MenuItemLabel *okMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::hideNoti, this));
                okMenu->setPosition(_bg->getContentSize().width / 2, _bg->getContentSize().height * 6.6 / 16);
                _bg->getChildByName("dumpMenu")->addChild(okMenu);
                lb->runAction(FadeIn::create(0.3));
            }), NULL));
        }
    } else if (mnSend->getTag() == 3) {
        if (UserDefault::getInstance()->getIntegerForKey(hardFlag, 0)) {
            UserDefault::getInstance()->setIntegerForKey(selectLevel, 3);
            this->aniHide(sender);
        } else {
            MenuItemSprite *dumpMenu = MenuItemSprite::create(Sprite::create("noteLevel.png"), NULL);
            dumpMenu->setPosition(_bg->getContentSize().width /2,  _bg->getContentSize().height * 2 );
            Menu *menu = Menu::create(dumpMenu, NULL);
            menu->setPosition(0,0);
            menu->setName("dumpMenu");
            _bg->addChild(menu,0,1);
            dumpMenu->runAction( Sequence::create(EaseBackOut::create(MoveTo::create(0.3, Point(_bg->getContentSize().width / 2, _bg->getContentSize().height / 2))), DelayTime::create(0.2),CallFunc::create([&] {
                Label *lb = Label::createWithSystemFont("PLEASE COMPLETE NORMAL MODE FIRST!", "Roboto-Thin", 24);
                lb->setPosition(_bg->getContentSize().width /2, _bg->getContentSize().height * 9.4 / 16);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                _bg->getChildByName("dumpMenu")->addChild(MenuItemLabel::create(lb));
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("OR", "Roboto-Thin", 24);
                lb->setPosition(_bg->getContentSize().width /2, _bg->getContentSize().height * 8.6 / 16);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                _bg->getChildByName("dumpMenu")->addChild(MenuItemLabel::create(lb));
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("TOUCH TO UNLOCK WITH 0.99$", "Roboto-Thin", 24);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                MenuItemLabel *buyMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::buyUnlockLevel, this));
                buyMenu->setPosition(_bg->getContentSize().width / 2, _bg->getContentSize().height * 7.8 / 16);
                buyMenu->setTag(4);
                _bg->getChildByName("dumpMenu")->addChild(buyMenu);
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("OK", "Roboto-Thin", 36);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                MenuItemLabel *okMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::hideNoti, this));
                okMenu->setPosition(_bg->getContentSize().width / 2, _bg->getContentSize().height * 6.6 / 16);
                _bg->getChildByName("dumpMenu")->addChild(okMenu);
                lb->runAction(FadeIn::create(0.3));
            }), NULL));

        }
    } else if (mnSend->getTag() == 4) {
        if (UserDefault::getInstance()->getIntegerForKey(insaneFlag, 0)) {
            UserDefault::getInstance()->setIntegerForKey(selectLevel, 4);
            this->aniHide(sender);
        } else {
            MenuItemSprite *dumpMenu = MenuItemSprite::create(Sprite::create("noteLevel.png"), NULL);
            dumpMenu->setPosition(_bg->getContentSize().width /2,  _bg->getContentSize().height * 2 );
            Menu *menu = Menu::create(dumpMenu, NULL);
            menu->setPosition(0,0);
            menu->setName("dumpMenu");
            _bg->addChild(menu,0,1);
            dumpMenu->runAction( Sequence::create(EaseBackOut::create(MoveTo::create(0.3, Point(_bg->getContentSize().width / 2, _bg->getContentSize().height / 2))), DelayTime::create(0.2),CallFunc::create([&] {
                Label *lb = Label::createWithSystemFont("PLEASE COMPLETE HARD MODE FIRST!", "Roboto-Thin", 24);
                lb->setPosition(_bg->getContentSize().width /2, _bg->getContentSize().height * 9.4 / 16);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                _bg->getChildByName("dumpMenu")->addChild(MenuItemLabel::create(lb));
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("OR", "Roboto-Thin", 24);
                lb->setPosition(_bg->getContentSize().width /2, _bg->getContentSize().height * 8.6 / 16);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                _bg->getChildByName("dumpMenu")->addChild(MenuItemLabel::create(lb));
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("TOUCH TO UNLOCK WITH 1.99$", "Roboto-Thin", 24);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                MenuItemLabel *buyMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::buyUnlockLevel, this));
                buyMenu->setTag(5);
                buyMenu->setPosition(_bg->getContentSize().width / 2, _bg->getContentSize().height * 7.8 / 16);
                _bg->getChildByName("dumpMenu")->addChild(buyMenu);
                lb->runAction(FadeIn::create(0.3));
                
                lb = Label::createWithSystemFont("OK", "Roboto-Thin", 36);
                lb->setColor(Color3B::BLACK);
                lb->setOpacity(0);
                MenuItemLabel *okMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(SelectLevelLayer::hideNoti, this));
                okMenu->setPosition(_bg->getContentSize().width / 2, _bg->getContentSize().height * 6.6 / 16);
                _bg->getChildByName("dumpMenu")->addChild(okMenu);
                lb->runAction(FadeIn::create(0.3));

            }), NULL));

        }
    }
    if (delegate) {
        delegate->updateSelectLevel();
    }
}
void SelectLevelLayer::hideNoti(cocos2d::Ref *sender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    _bg->getChildByName("dumpMenu")->runAction(Sequence::create(EaseBackIn::create(MoveTo::create(0.3, Point(0, - 2* _bg->getContentSize().height))),CallFunc::create([&] {
        _bg->getChildByName("dumpMenu")->removeFromParentAndCleanup(true);
    }), NULL));
}
void SelectLevelLayer::buyUnlockLevel(cocos2d::Ref *sender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    MenuItemLabel *mlb = static_cast<MenuItemLabel*>(sender);
    CallBridge::getListIAPItem(mlb->getTag());
}