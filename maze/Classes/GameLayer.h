//
//  GameLayer.h
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/20/14.
//
//

#ifndef __SwingCopter__GameLayer__
#define __SwingCopter__GameLayer__

#include <iostream>
#include "cocos2d.h"
#include "IntroScene.h"
#include "Model.h"
#include "DrawMazeLayer.h"
#include "PlayerSprite.h"
#include "WingameLayer.h"
#include "GameFinishLayer.h"
#include "IAPLayer.h"

USING_NS_CC;

class GameLayer : public Layer, public DrawMazeLayerProtocol, public PlayerSpriteProtocol, public IAPLayerProtocol {
public:
	GameLayer();
	~GameLayer();
    
    bool init(int numRow, int numCol);
    void update(float dt);
    Size _winSize;
    static GameLayer* create(int numRow, int numCol);
    float _timeShake = 0.4;
    float _timmer,_score = 0;
    int _numRow = 2, _numCol = 2;
    bool _gameStatus = false, _playerMovingFlag = false;
    __Array *_maze;
    PlayerSprite *_player;
    DrawMazeLayer *_mazeLayer;
    // a selector callback
	/**
     * This layer need physical engine work
     */
	void setPhyWorld(PhysicsWorld* world){this->world = world;}
    void shakeScreen(float dt);
    void movePlayer(cocos2d::Ref* pSender);
    void hint(cocos2d::Ref* pSender);
    void aniGameStart();
    void winGame();
    void showWinLayer();
    void lostGame();
    Label *_aniLabel,*_timeLabel;
    LayerColor *_lc;
    void debug(cocos2d::Ref* pSender);
    Point _touchStart, _touchEnd;
    void changePlayerMovingStatus(bool flag);
    void updateSantaHat();
    void afterCapture(bool flag, const std::string& fileName);
    __String *_fileCapturePath = NULL;
private:
    PhysicsWorld *world;
    virtual void onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event);
    virtual void onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event);
    virtual void onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event);
    bool onContactBegin(PhysicsContact& contact);
};
#endif /* defined(__SwingCopter__GameLayer__) */
