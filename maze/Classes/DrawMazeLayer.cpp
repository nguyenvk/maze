//
//  DrawMazeLayer.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/8/14.
//
//

#include "DrawMazeLayer.h"
DrawMazeLayer* DrawMazeLayer::create(__Array *maze, int numRow, int numCol)
{
    DrawMazeLayer *pRet = new DrawMazeLayer();
    if (pRet && pRet->init(maze, numRow, numCol))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool DrawMazeLayer::init(__Array *maze, int numRow, int numCol)
{
    _winSize = Director::getInstance()->getVisibleSize();
    _maze = maze;
    _numCol = numCol; _numRow = numRow;
    float width = 620;
    float height = width * numCol / numRow;
    if ( !LayerColor::initWithColor(Color4B(255, 255, 255, 255), width, height))
    {
        return false;
    }
    DrawNode *dn = DrawNode::create();
    __Dictionary *dict;
    cellWidth = width / numRow;
    cellHeight = height / numCol;
    for (int i = 0; i < numRow; i ++) {
        for (int j = 0; j < numCol; j++) {
            dict = Model::getMazeData(_maze, i, j, numRow, numCol);
            if (dict) {
                if (static_cast<__Integer*>(dict->objectForKey(TOP))->getValue()) {
                    dn->drawSolidRect(Point(i * cellWidth, (j + 1) * cellHeight + 2), Point((i + 1) * cellWidth,(j + 1) * cellHeight - 2), Color4F::BLACK);
                }
                if (static_cast<__Integer*>(dict->objectForKey(RIGHT))->getValue()) {
                    dn->drawSolidRect(Point((i + 1)* cellWidth + 2, j * cellHeight), Point((i + 1) * cellWidth - 2,(j + 1) * cellHeight), Color4F::BLACK);
                }
                if (static_cast<__Integer*>(dict->objectForKey(DOWN))->getValue()) {
                    dn->drawSolidRect(Point((i + 1) * cellWidth, j * cellHeight + 2), Point(i * cellWidth,j * cellHeight - 2), Color4F::BLACK);
                }
                if (static_cast<__Integer*>(dict->objectForKey(LEFT))->getValue()) {
                    dn->drawSolidRect(Point(i * cellWidth + 2, (j + 1) * cellHeight), Point(i * cellWidth - 2,j * cellHeight), Color4F::BLACK);
                }
            }
        }
    }
    
    this->addChild(dn);
    return true;
}
void DrawMazeLayer::setMazeCampain(Point sPoint, Point ePoint) {
    __Dictionary *startPoint = __Dictionary::create();
    startPoint->setObject(__Integer::create(sPoint.x), "x");
    startPoint->setObject(__Integer::create(sPoint.y), "y");
    __Dictionary *endPoint = __Dictionary::create();
    endPoint->setObject(__Integer::create(ePoint.x), "x");
    endPoint->setObject(__Integer::create(ePoint.y), "y");
    
    _resultPath = NULL;
    _resultPath = Model::sloveMaze(_maze, startPoint, endPoint, _numRow, _numCol);
    if (_resultPath) {
        _resultPath->retain();
    }
}
void DrawMazeLayer::drawSloveMaze(float dt) {
    if (!_resultPath) return;
    __Dictionary *dict;
    dict = static_cast<__Dictionary*>(_resultPath->getObjectAtIndex(_indexSloveDraw));
    int x = static_cast<__Integer*>(dict->objectForKey("x"))->getValue();
    int y = static_cast<__Integer*>(dict->objectForKey("y"))->getValue();
    if (_indexSloveDraw + 1 >= _resultPath->count()) {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("winLevel1.mp3");
        Label *lb = Label::createWithSystemFont("", fontAs, 450  / _numRow);
        lb->setColor(Color3B::RED);
        lb->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
        this->addChild(lb);
        _resultPath->release();
        Vector<SpriteFrame*> aniFame;
        for (int i = 0; i < 10; i++) {
            aniFame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(__String::createWithFormat("GFO_Characters-image_0_%d",i)->getCString()));
        }
        this->getChildByName("reindderWin")->runAction(RepeatForever::create(Animate::create(Animation::createWithSpriteFrames(aniFame,0.1))));
        this->getChildByName("reindderWin")->runAction(ScaleTo::create(1, -2, 2));
        this->getChildByName("reindderWin")->runAction(Sequence::create(MoveTo::create(1, Point(this->getContentSize().width / 2, this->getContentSize().height / 2)), DelayTime::create(0.7), CallFunc::create([&] {
            delegate->showWinLayer();
        }), NULL));
    } else {
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("coin.caf");
        dict = static_cast<__Dictionary*>(_resultPath->getObjectAtIndex(_indexSloveDraw + 1));
        int xNext = static_cast<__Integer*>(dict->objectForKey("x"))->getValue();
        int yNext = static_cast<__Integer*>(dict->objectForKey("y"))->getValue();
        Label *lb;
        if (xNext > x && yNext == y) {
            //right
            lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
        } else if (xNext == x && yNext > y) {
            //up
            lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
        } else if (xNext < x && yNext == y) {
            //left
            lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
        } else {
            //down
            lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
        }
        lb->setColor(Color3B::RED);
        lb->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
        this->addChild(lb);
        //lb->runAction(Sequence::create(EaseBackIn::create(ScaleTo::create(0.8, 1.9)), EaseBackOut::create(ScaleTo::create(0.8, 0.6)),EaseBackIn::create(ScaleTo::create(0.8, 1.5)), EaseBackOut::create(ScaleTo::create(0.8, 0.8)),EaseBackIn::create(ScaleTo::create(0.8, 1.2)), EaseBackOut::create(ScaleTo::create(0.8, 1)), NULL));
    }
    _indexSloveDraw++;
    if (_indexSloveDraw == 1) {
        this->schedule(schedule_selector(DrawMazeLayer::drawSloveMaze), timeDelaySlove, _resultPath->count() - 2, 0);
    }
//    if (_indexSloveDraw < _resultPath->count()) {
//        this->scheduleOnce(schedule_selector(DrawMazeLayer::drawSloveMaze), timeDelaySlove);
//    }
}
void DrawMazeLayer::autoMove(float dt) {
    if (!_resultPath) return;
    __Dictionary *dict;
    PlayerSprite *player = static_cast<PlayerSprite*>(this->getChildByTag(10));
    dict = static_cast<__Dictionary*>(_resultPath->getObjectAtIndex(_indexAutoMove));
    int x = static_cast<__Integer*>(dict->objectForKey("x"))->getValue();
    int y = static_cast<__Integer*>(dict->objectForKey("y"))->getValue();
    player->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
    if (_indexAutoMove + 1 >= _resultPath->count()) {
        player->delegate->winGame();
    }
    _indexAutoMove++;
    if (_indexAutoMove == 1) {
        this->schedule(schedule_selector(DrawMazeLayer::autoMove), timeDelaySlove, _resultPath->count() - 2, 0);
    }
}
void DrawMazeLayer::runDebug(cocos2d::__Array *maze, cocos2d::__Dictionary *startPoint, cocos2d::__Dictionary *endPoint, int numRow, int numCol){
    __Array *openSet = __Array::createWithCapacity(numCol * numRow);
    __Array *resultPath = NULL;
    Vector<__Integer*>  arrMark(numCol * numRow - 1);
    for (int i = 0; i < numRow; i++) {
        for (int j = 0 ; j < numCol; j ++) {
            arrMark.insert(i * numCol + j, __Integer::create(0));
        }
    }
    int i =0;
    openSet->addObject(__Array::createWithObject(startPoint));
    arrMark.insert(0, __Integer::create(0));
    int breakKey = 1;
    while (openSet->count() > 0 && breakKey < 10000) {
        breakKey++;
        i++;
        __Array *current = static_cast<__Array*>(openSet->getObjectAtIndex(0));
        __Dictionary *lastPoint  = static_cast<__Dictionary*>(current->getLastObject());
        int x = static_cast<__Integer*>(lastPoint->objectForKey("x"))->getValue();
        int y = static_cast<__Integer*>(lastPoint->objectForKey("y"))->getValue();
        
        Label *lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
        lb->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
        lb->setColor(Color3B::GREEN);
        this->addChild(lb);
        
        CCLOG(" lan thu %d, so set %zd, vao x = %d, y = %d", i, openSet->count(),x,y);
        __Dictionary *infoPoint = Model::getMazeData(maze, x , y, numRow, numCol);
        //do top
        int nextX = x;
        int nextY = y + 1;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug TOP val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(TOP))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(TOP))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
        //do right
        nextX = x + 1;
        nextY = y;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug RIGHT val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(RIGHT))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(RIGHT))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
        //        for (int i = 0; i < numRow; i++) {
        //            for (int j = 0 ; j < numCol; j ++) {
        //                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
        //                    CCLOG("Error at %d AND %d",i,j);
        //                }
        //            }
        //        }
        
        //do down
        nextX = x;
        nextY = y - 1;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug DOWN val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(DOWN))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(DOWN))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
        //        for (int i = 0; i < numRow; i++) {
        //            for (int j = 0 ; j < numCol; j ++) {
        //                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
        //                    CCLOG("Error at %d AND %d",i,j);
        //                }
        //            }
        //        }
        
        //do left
        nextX = x - 1;
        nextY = y;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug LEFT val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(LEFT))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(LEFT))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
        //        for (int i = 0; i < numRow; i++) {
        //            for (int j = 0 ; j < numCol; j ++) {
        //                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
        //                    CCLOG("Error at %d AND %d",i,j);
        //                }
        //            }
        //        }
        openSet->removeObjectAtIndex(0);
    }


}
void DrawMazeLayer::addReindder() {
    Sprite *sp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("GFO_Characters-image_2_0"));
    sp->setName("reindder");
    sp->setPosition(cellWidth * (_numRow - 1) + cellWidth / 2, cellHeight * (_numCol - 1) + cellHeight / 2);
    sp->setScale( - cellWidth / sp->getContentSize().width, cellHeight / sp->getContentSize().height);
    this->addChild(sp,9999);
    Vector<SpriteFrame*> aniFame;
    for (int i = 0; i < 10; i++) {
        aniFame.pushBack(SpriteFrameCache::getInstance()->getSpriteFrameByName(__String::createWithFormat("GFO_Characters-image_2_%d",i)->getCString()));
    }
    sp->runAction(RepeatForever::create(Animate::create(Animation::createWithSpriteFrames(aniFame,0.1))));
}
void DrawMazeLayer::addWinReindder() {
    this->getChildByName("reindder")->removeFromParentAndCleanup(true);
    this->getChildByTag(10)->removeFromParentAndCleanup(true);
    Sprite *sp = Sprite::createWithSpriteFrame(SpriteFrameCache::getInstance()->getSpriteFrameByName("GFO_Characters-image_0_0"));
    sp->setName("reindderWin");
    sp->setPosition(cellWidth * (_numRow - 1) + cellWidth / 2, cellHeight * (_numCol - 1) + cellHeight / 2);
    sp->setScale( - cellWidth / sp->getContentSize().width, cellHeight / sp->getContentSize().height);
    this->addChild(sp,9999);
}
bool DrawMazeLayer::drawHint() {
    if (!_resultPath || _timeHint >=2) return false;
    __Dictionary *dict;
    for (int i = 0; i < _resultPath->count() - 1; i++) {
        if (_timeHint == 0) {
            if (i < _resultPath->count() / 2 - 1) {
                continue;
            }
        }
        dict = static_cast<__Dictionary*>(_resultPath->getObjectAtIndex(i));
        int x = static_cast<__Integer*>(dict->objectForKey("x"))->getValue();
        int y = static_cast<__Integer*>(dict->objectForKey("y"))->getValue();
        
        if (i + 1 >= _resultPath->count()) {
            Label *lb = Label::createWithSystemFont("", fontAs, 450  / _numRow);
            lb->setColor(Color3B::RED);
            lb->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
            this->addChild(lb);
            _resultPath->release();
        } else {
            dict = static_cast<__Dictionary*>(_resultPath->getObjectAtIndex(i + 1));
            int xNext = static_cast<__Integer*>(dict->objectForKey("x"))->getValue();
            int yNext = static_cast<__Integer*>(dict->objectForKey("y"))->getValue();
            Label *lb;
            if (xNext > x && yNext == y) {
                //right
                lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
            } else if (xNext == x && yNext > y) {
                //up
                lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
            } else if (xNext < x && yNext == y) {
                //left
                lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
            } else {
                //down
                lb = Label::createWithSystemFont("", fontAs, 450 / _numRow);
            }
            lb->setColor(Color3B(33,126,63));
            lb->setPosition(x * cellWidth + cellWidth /2, y * cellHeight + cellHeight / 2);
            this->addChild(lb);
        }
    }
    _timeHint++;
    return true;
}
void DrawMazeLayer::addSantaHat() {
    //cu moi o 4x4 thi co 1 santahat, ko lam tron len
    int numX = _numRow / 5;
    int numY = _numCol / 5;
    Sprite *santaHatSpr;
    _santahatArrSpr = __Array::createWithCapacity(numX * numY);
    for (int i = 0; i < numX; i++) {
        for (int j = 0; j < numY; j++) {
            int randX = (rand() % 5) + i * 5;
            int randY = (rand() % 5) + j * 5;
            _santahatLoc.pushBack(__Array::create(__Integer::create(randX), __Integer::create(randY), NULL));
            santaHatSpr = Sprite::create("santas_hat.png");
            santaHatSpr->setScale(cellWidth / santaHatSpr->getContentSize().height * 0.7);
            santaHatSpr->setPosition(randX * cellWidth + cellWidth / 2, randY * cellHeight + cellHeight / 2);
            this->addChild(santaHatSpr);
            _santahatArrSpr->addObject(santaHatSpr);
        }
    }
    _santahatArrSpr->retain();
}












