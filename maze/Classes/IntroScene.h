//
//  IntroScene.h
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/20/14.
//
//

#ifndef __SwingCopter__IntroScene__
#define __SwingCopter__IntroScene__

#include <iostream>
#include "cocos2d.h"
#include "CallBridge.h"
#include "Model.h"
#include "GameScene.h"
#include "IAPLayer.h"
#include "SelectLevelLayer.h"

USING_NS_CC;

class IntroScene : public cocos2d::Layer, SelectLevelLayerProtocol, IAPLayerProtocol
{
private:
    // a selector callback
    void menuRateCallback(cocos2d::Ref* pSender);
    void menuLeaderboardCallback(cocos2d::Ref* pSender);
    void menuRemoveAdsCallback(cocos2d::Ref* pSender);
    void startGameEasy(cocos2d::Ref* pSender);
    void moreGame(cocos2d::Ref* pSender);
    void shareFacebook(cocos2d::Ref* pSender);
    void choiseLevel(cocos2d::Ref* pSender);
    void showIAP(cocos2d::Ref* pSender);
public:
    // there's no 'id' in cpp, so we recommend returning the class instance pointer
    static cocos2d::Scene* createScene();
    
    // Here's a difference. Method 'init' in cocos2d-x returns bool, instead of returning 'id' in cocos2d-iphone
    virtual bool init();
    Sprite *_volumeSprite;
    Size _winSize;
    void updateBall();
    SelectLevelLayer *_selectLevelLayer;
    void updateSelectLevel();
    void createMenu();
    void updateSantaHat();
    void switchSoud(cocos2d::Ref* pSender);
    // implement the "static create()" method manually
    CREATE_FUNC(IntroScene);
};
#endif /* defined(__SwingCopter__IntroScene__) */
