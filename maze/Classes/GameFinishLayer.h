//
//  GameFinishLayer.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/10/14.
//
//

#ifndef __maze__GameFinishLayer__
#define __maze__GameFinishLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "Model.h"
#include "GameScene.h"
#include "IntroScene.h"
#include "IAPLayer.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

class GameFinishLayer: public LayerColor, IAPLayerProtocol {
public:
    virtual bool init();
    static GameFinishLayer* create();
    Size _winSize;
    LayerColor *_mainChild;
    void retryLevel(cocos2d::Ref* pSender);
    void backHome(cocos2d::Ref* pSender);
    void shareFacebook(cocos2d::Ref* pSender);
    void menuLeaderboardCallback(cocos2d::Ref* pSender);
    void menuRateCallback(cocos2d::Ref* pSender);
    int _numRow,_numCol,_score,_currentSize;
    float _timmer, _timeAni = 2;
    void setData(int numRow, int numCol, float timmer, int score);
    void runAni();
    void updateSantaHat();
};
#endif /* defined(__maze__GameFinishLayer__) */
