//
//  IAPLayer.h
//  RollingBall
//
//  Created by Nguyen Vo Kim on 11/26/14.
//
//

#ifndef __RollingBall__IAPLayer__
#define __RollingBall__IAPLayer__

#include <stdio.h>
#include "Model.h"
#include "CallBridge.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
class IAPLayerProtocol {
public:
    virtual void updateSantaHat() = 0;
};
class IAPLayer: public LayerColor {
public:
    virtual bool init();
    static IAPLayer* create();
    //void update(float time);
    Size _winSize;
    void buyItem(cocos2d::Ref* pSender);
    void closeIAP(cocos2d::Ref* pSender);
    void restonePurchase(cocos2d::Ref* pSender);
    void checkRemoveMenuAds();
    void runAniShow();
    IAPLayerProtocol *delegate;
    void createMenu();
    Sprite *_bg;
};

#endif /* defined(__RollingBall__IAPLayer__) */
