//
//  SelectLevelLayer.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/11/14.
//
//

#ifndef __maze__SelectLevelLayer__
#define __maze__SelectLevelLayer__

#include <iostream>
#include "cocos2d.h"
#include "Model.h"
#include "CallBridge.h"
#include "SimpleAudioEngine.h"

class SelectLevelLayerProtocol {
public:
    virtual void updateSelectLevel() = 0;
};

class SelectLevelLayer: public LayerColor {
public:
    virtual bool init();
    static SelectLevelLayer* create();
    Size _winSize;
    Sprite *_bg;
    void aniShow();
    void aniHide(Ref *sender);
    void createMenu();
    void changeLevel(Ref *sender);
    void hideNoti(Ref *sender = NULL);
    void buyUnlockLevel(cocos2d::Ref *sender);
    SelectLevelLayerProtocol *delegate;
};
#endif /* defined(__maze__SelectLevelLayer__) */
