//
//  Model.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/8/14.
//
//

#include "Model.h"

__Array* Model::gentMazeData(int width, int height) {
    int i,j;
    __Array *maze = __Array::createWithCapacity(width);
    for (i = 0; i < width;  i++) {
        __Array *mazeRow = __Array::createWithCapacity(height);
        for (j = 0 ; j < height; j++) {
            __Dictionary *dict = __Dictionary::create();
            dict->setObject(__Integer::create(1), TOP);
            dict->setObject(__Integer::create(1), RIGHT);
            dict->setObject(__Integer::create(1), DOWN);
            dict->setObject(__Integer::create(1), LEFT);
            dict->setObject(__Integer::create(0), ISVISIT);
            mazeRow->addObject(dict);
        }
        maze->addObject(mazeRow);
    }
    int x = 0, y = 0, num = 0, totalCell = width * height;
    __Array *history = __Array::createWithCapacity(width * height);
    int breakKey = 1;
    while (num < totalCell && breakKey < 10000) {
        breakKey++;
        __Dictionary *dict = Model::getMazeData(maze, x, y, width, height);
        dict->setObject(__Integer::create(1), ISVISIT);
        __Array *arrPick = __Array::create();
        //top maze
        dict = Model::getMazeData(maze, x, y + 1, width, height);
        if (dict && static_cast<__Integer*>(dict->objectForKey(ISVISIT))->getValue() == 0) {
            arrPick->addObject(__Array::create(__Integer::create(x), __Integer::create(y + 1), __Integer::create(KEY_TOP), NULL));
            
        }
        //right maze
        dict = Model::getMazeData(maze, x + 1, y, width, height);
        if (dict && static_cast<__Integer*>(dict->objectForKey(ISVISIT))->getValue() == 0) {
            arrPick->addObject(__Array::create(__Integer::create(x + 1), __Integer::create(y), __Integer::create(KEY_RIGHT), NULL));
            
        }
        //down maze
        dict = Model::getMazeData(maze, x, y - 1, width, height);
        if (dict && static_cast<__Integer*>(dict->objectForKey(ISVISIT))->getValue() == 0) {
            arrPick->addObject(__Array::create(__Integer::create(x), __Integer::create(y - 1), __Integer::create(KEY_DOWN), NULL));
            
        }
        //left maze
        dict = Model::getMazeData(maze, x - 1, y, width, height);
        if (dict && static_cast<__Integer*>(dict->objectForKey(ISVISIT))->getValue() == 0) {
            arrPick->addObject(__Array::create(__Integer::create(x - 1), __Integer::create(y), __Integer::create(KEY_LEFT), NULL));
            
        }
        if (arrPick->count() == 0) {
            __Array *arrDump = static_cast<__Array*>(history->getLastObject());
            if (!arrDump) {
                break;
            }
            history->removeLastObject();
            x = static_cast<__Integer*>(arrDump->getObjectAtIndex(0))->getValue();
            y = static_cast<__Integer*>(arrDump->getObjectAtIndex(1))->getValue();
        } else {
            num++;
            history->addObject(__Array::create(__Integer::create(x), __Integer::create(y), NULL));
            __Array *vec = static_cast<__Array*>(arrPick->getRandomObject());
            if (static_cast<__Integer*>(vec->getObjectAtIndex(2))->getValue() == KEY_TOP) {
                dict = Model::getMazeData(maze, x, y, width, height);
                dict->setObject(__Integer::create(0), TOP);
                dict = Model::getMazeData(maze, static_cast<__Integer*>(vec->getObjectAtIndex(0))->getValue(), static_cast<__Integer*>(vec->getObjectAtIndex(1))->getValue(), width, height);
                dict->setObject(__Integer::create(0), DOWN);
            }
            if (static_cast<__Integer*>(vec->getObjectAtIndex(2))->getValue() == KEY_RIGHT) {
                dict = Model::getMazeData(maze, x, y, width, height);
                dict->setObject(__Integer::create(0), RIGHT);
                dict = Model::getMazeData(maze, static_cast<__Integer*>(vec->getObjectAtIndex(0))->getValue(), static_cast<__Integer*>(vec->getObjectAtIndex(1))->getValue(), width, height);
                dict->setObject(__Integer::create(0), LEFT);
            }
            if (static_cast<__Integer*>(vec->getObjectAtIndex(2))->getValue() == KEY_DOWN) {
                dict = Model::getMazeData(maze, x, y, width, height);
                dict->setObject(__Integer::create(0), DOWN);
                dict = Model::getMazeData(maze, static_cast<__Integer*>(vec->getObjectAtIndex(0))->getValue(), static_cast<__Integer*>(vec->getObjectAtIndex(1))->getValue(), width, height);
                dict->setObject(__Integer::create(0), TOP);
            }
            if (static_cast<__Integer*>(vec->getObjectAtIndex(2))->getValue() == KEY_LEFT) {
                dict = Model::getMazeData(maze, x, y, width, height);
                dict->setObject(__Integer::create(0), LEFT);
                dict = Model::getMazeData(maze, static_cast<__Integer*>(vec->getObjectAtIndex(0))->getValue(), static_cast<__Integer*>(vec->getObjectAtIndex(1))->getValue(), width, height);
                dict->setObject(__Integer::create(0), RIGHT);
            }
            x = static_cast<__Integer*>(vec->getObjectAtIndex(0))->getValue();
            y = static_cast<__Integer*>(vec->getObjectAtIndex(1))->getValue();
        }
    }
    return maze;
}
__Dictionary* Model::getMazeData(__Array *maze, int x, int y, int width, int height) {
    if (x < 0 || x >= width || y < 0 || y >= height) {
        return NULL;
    }
    Ref *dict = static_cast<__Array*>(maze->getObjectAtIndex(x))->getObjectAtIndex(y);
    if (dict) {
        return static_cast<__Dictionary*>(dict);
    } else {
        return NULL;
    }
}
__Array* Model::sloveMaze(__Array *maze, __Dictionary *startPoint, __Dictionary *endPoint, int numRow, int numCol) {
    __Array *openSet = __Array::createWithCapacity(numCol * numRow);
    __Array *resultPath = NULL;
    Vector<__Integer*>  arrMark(numCol * numRow - 1);
    for (int i = 0; i < numRow; i++) {
        for (int j = 0 ; j < numCol; j ++) {
            arrMark.insert(i * numCol + j, __Integer::create(0));
        }
    }
    int i =0;
    openSet->addObject(__Array::createWithObject(startPoint));
    arrMark.insert(0, __Integer::create(0));
    int breakKey = 1;
    while (openSet->count() > 0 && breakKey < 10000) {
        breakKey++;
        i++;
        __Array *current = static_cast<__Array*>(openSet->getObjectAtIndex(0));
        __Dictionary *lastPoint  = static_cast<__Dictionary*>(current->getLastObject());
        int x = static_cast<__Integer*>(lastPoint->objectForKey("x"))->getValue();
        int y = static_cast<__Integer*>(lastPoint->objectForKey("y"))->getValue();
        CCLOG(" lan thu %d, so set %zd, vao x = %d, y = %d", i, openSet->count(),x,y);
        __Dictionary *infoPoint = Model::getMazeData(maze, x , y, numRow, numCol);
        //do top
        int nextX = x;
        int nextY = y + 1;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug TOP val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(TOP))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(TOP))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }

        //do right
        nextX = x + 1;
        nextY = y;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug RIGHT val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(RIGHT))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(RIGHT))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
//        for (int i = 0; i < numRow; i++) {
//            for (int j = 0 ; j < numCol; j ++) {
//                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
//                    CCLOG("Error at %d AND %d",i,j);
//                }
//            }
//        }
        
        //do down
        nextX = x;
        nextY = y - 1;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug DOWN val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(DOWN))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(DOWN))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
//        for (int i = 0; i < numRow; i++) {
//            for (int j = 0 ; j < numCol; j ++) {
//                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
//                    CCLOG("Error at %d AND %d",i,j);
//                }
//            }
//        }
        
        //do left
        nextX = x - 1;
        nextY = y;
        if (nextX >= 0 && nextX < numRow && nextY >=0 && nextY < numCol) {
            CCLOG("Debug LEFT val1 = %d, val2 = %d, phan tu array %d va %d",static_cast<__Integer*>(infoPoint->objectForKey(LEFT))->getValue(),arrMark.at(nextX * numCol + nextY)->getValue(), nextX, nextY);
            if (static_cast<__Integer*>(infoPoint->objectForKey(LEFT))->getValue() == 0 && arrMark.at(nextX * numCol + nextY)->getValue() == 0 && Model::getMazeData(maze, nextX, nextY, numRow, numCol)) {
                arrMark.replace(nextX * numCol + nextY, __Integer::create(1));
                __Array *dump = current->clone();
                __Dictionary *dumpDict = __Dictionary::create();
                dumpDict->setObject(__Integer::create(nextX), "x");
                dumpDict->setObject(__Integer::create(nextY), "y");
                dump->addObject(dumpDict);
                openSet->addObject(dump);
                if (nextX == static_cast<__Integer*>(endPoint->objectForKey("x"))->getValue() && nextY == static_cast<__Integer*>(endPoint->objectForKey("y"))->getValue()) {
                    resultPath = __Array::createWithArray(dump);
                    break;
                }
            }
        }
//        for (int i = 0; i < numRow; i++) {
//            for (int j = 0 ; j < numCol; j ++) {
//                if (arrMark[i * numRow + j] !=0 && arrMark[i * numRow + j] !=1) {
//                    CCLOG("Error at %d AND %d",i,j);
//                }
//            }
//        }
        openSet->removeObjectAtIndex(0);
    }
    return resultPath;
}
float Model::calMazeTime(int numRow, int numCol, int level) {
    if (level == 1) {
        return numRow * numCol * 0.6;
    } else if (level == 2) {
        return numRow * numCol * 0.53;
    } else if (level == 3) {
        return numRow * numCol * 0.47;
    } else {
        return numRow * numCol * 0.4;
    }
}





