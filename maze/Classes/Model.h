//
//  Model.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/8/14.
//
//

#ifndef __maze__Model__
#define __maze__Model__

#include <stdio.h>
#include <vector>
#include "cocos2d.h"

#define TOP  "top"
#define RIGHT  "right"
#define DOWN  "down"
#define LEFT  "left"
#define ISVISIT  "isVisit"
#define KEY_TOP  0
#define KEY_RIGHT  1
#define KEY_DOWN  2
#define KEY_LEFT  3
#define fontAs "FontAwesome"
#define scoreEasy "scoreEasy"
#define scoreNormal "scoreNormal"
#define scoreHard "scoreHard"
#define scoreInsane "scoreInsane"
#define easyFlag "easyFlag"
#define normalFlag "normalFlag"
#define hardFlag "hardFlag"
#define insaneFlag "insaneFlag"
#define selectLevel "selectLevel"
#define currentSizeEasy "currentSizeEasy"
#define currentSizeNormal "currentSizeNormal"
#define currentSizeHard "currentSizeHard"
#define currentSizeInsane "currentSizeInsane"
#define timeDelaySlove 0.05
#define santaHat "santaHat"
#define medalEasy "medalEasy"
#define medalNormal "medalNormal"
#define medalHard "medalHard"
#define medalInsane "medalInsane"
#define numRetry "numRetry"
USING_NS_CC;

class Model {
public:
    static __Array* gentMazeData(int width = 3, int height = 3);
    static __Dictionary* getMazeData(__Array *maze, int x, int y, int width, int height);
    static __Array* sloveMaze( __Array *maze, __Dictionary *startPoint, __Dictionary *endPoint, int numRow, int numCol);
    static float calMazeTime(int numRow, int numCol, int level);
};

#endif /* defined(__maze__Model__) */
