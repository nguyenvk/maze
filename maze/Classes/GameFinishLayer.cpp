//
//  GameFinishLayer.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/10/14.
//
//

#include "GameFinishLayer.h"
GameFinishLayer* GameFinishLayer::create()
{
    GameFinishLayer *pRet = new GameFinishLayer();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GameFinishLayer::init()
{
    _winSize = Director::getInstance()->getVisibleSize();
    if ( !LayerColor::initWithColor(Color4B::RED, _winSize.width, _winSize.height))
    {
        return false;
    }
    _mainChild = LayerColor::create(Color4B(255,255,255,255), _winSize.width, _winSize.height);
    this->setName("GameFinishLayer");
    
    Label *lb = Label::createWithSystemFont("FAIL!", "Roboto-Bold", 128);
    lb->setColor(Color3B::RED);
    lb->setPosition(_winSize.width/2, _winSize.height * 3 / 4);
    _mainChild->addChild(lb);
    lb = Label::createWithSystemFont("SO SAD..", "Roboto-Light", 96);
    lb->setColor(Color3B::RED);
    lb->setPosition(_winSize.width/2, _winSize.height * 2.5 / 4);
    _mainChild->addChild(lb);
    
    _mainChild->setAnchorPoint(Point::ANCHOR_MIDDLE_TOP);
    _mainChild->setPosition(0, 0);
    _mainChild->setScale(0.01);
    _mainChild->setOpacity(0);
    this->addChild(_mainChild);
    
    int numberRetry = UserDefault::getInstance()->getIntegerForKey(numRetry, 0);
    
    MenuItemLabel *leaderMenu = MenuItemLabel::create(Sprite::create("scoresButton.png"), CC_CALLBACK_1(GameFinishLayer::menuLeaderboardCallback, this));
    leaderMenu->setPosition(_winSize.width * 0.92 / 3, _winSize.height * 2.1 / 5);
    
    MenuItemLabel *retryMenu = MenuItemLabel::create(Sprite::create("retryButton.png"), CC_CALLBACK_1(GameFinishLayer::retryLevel, this));
    retryMenu->setPosition(_winSize.width * 2.08 / 3, _winSize.height * 2.1 / 5);
    if (numberRetry < 5) {
        lb = Label::createWithSystemFont(__String::createWithFormat("Use %d Santa Hat", (numberRetry) * (numberRetry + 1) * 10 + 10)->getCString(), "Roboto-Regular", 18);
        lb->setPosition(retryMenu->getContentSize().width / 2 , retryMenu->getContentSize().height * 0.8 / 4);
        lb->setColor(Color3B::RED);
        retryMenu->addChild(lb);
    }
    
    if (numberRetry >=5 ) {
        lb = Label::createWithSystemFont(__String::createWithFormat("It truly over, you can not retry anymore!")->getCString(), "Roboto-Light", 36);
    } else {
        lb = Label::createWithSystemFont(__String::createWithFormat("You can retry %d time(s)!", 5 - UserDefault::getInstance()->getIntegerForKey(numRetry, 0))->getCString(), "Roboto-Light", 36);
    }
    lb->setPosition(_winSize.width / 2, _winSize.height * 2.7 / 5);
    lb->setColor(Color3B::BLACK);
    _mainChild->addChild(lb);
    
    MenuItemLabel *shareMenu = MenuItemLabel::create(Sprite::create("shareButton.png"), CC_CALLBACK_1(GameFinishLayer::shareFacebook, this));
    shareMenu->setPosition(_winSize.width * 1 / 3, _winSize.height * 1.4 / 5);
    shareMenu->setScale(0.8);
    
    MenuItemLabel *rateMenu = MenuItemLabel::create(Sprite::create("rateButton.png"), CC_CALLBACK_1(GameFinishLayer::menuRateCallback, this));
    rateMenu->setPosition(_winSize.width * 2 / 3, _winSize.height * 1.4 / 5);
    rateMenu->setScale(0.8);
    
    lb = Label::createWithSystemFont("Back Home", "Roboto-Regular", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *backHome = MenuItemLabel::create(lb, CC_CALLBACK_1(GameFinishLayer::backHome, this));
    backHome->setPosition(_winSize.width / 2, _winSize.height * 1 / 5);
    
    Menu *menu = Menu::create(leaderMenu,retryMenu,shareMenu,rateMenu,backHome, NULL);
    menu->setPosition(Point::ZERO);
    _mainChild->addChild(menu);
    
    Sprite *sprite = Sprite::create("santas_hat.png");
    sprite->setPosition(_winSize.width * 1.3 / 3, _winSize.height * 9.5 / 10);
    _mainChild->addChild(sprite);
    Label *labelSantaHat = Label::createWithSystemFont(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString(), "Roboto-Light", 48);
    labelSantaHat->setColor(Color3B::BLACK);
    labelSantaHat->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
    labelSantaHat->setPosition(sprite->getPositionX() + sprite->getContentSize().width /2  + 10, sprite->getPositionY());
    _mainChild->addChild(labelSantaHat,1,99);
    
    int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    if (level == 1) {
        _currentSize = UserDefault::getInstance()->getIntegerForKey(currentSizeEasy, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeEasy, 0);
        UserDefault::getInstance()->flush();
    } else if (level == 2) {
        _currentSize = UserDefault::getInstance()->getIntegerForKey(currentSizeNormal, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeNormal, 0);
        UserDefault::getInstance()->flush();
    } else if (level == 3) {
        _currentSize = UserDefault::getInstance()->getIntegerForKey(currentSizeHard, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeHard, 0);
        UserDefault::getInstance()->flush();
    } else {
        _currentSize = UserDefault::getInstance()->getIntegerForKey(currentSizeInsane, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeInsane, 0);
        UserDefault::getInstance()->flush();
    }
    
    return true;
}
void GameFinishLayer::retryLevel(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    int numberRetry = UserDefault::getInstance()->getIntegerForKey(numRetry, 0);
    int needHat = (numberRetry) * (numberRetry + 1) * 10 + 10;
    int numSantaHat = UserDefault::getInstance()->getIntegerForKey(santaHat, 0);
    if (numSantaHat < needHat) {
        IAPLayer *iap = IAPLayer::create();
        iap->setPosition(0,0);
        iap->delegate = this;
        this->getParent()->addChild(iap,9999,9999);
        iap->runAniShow();
    } else {
        int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
        if (level == 1) {
            UserDefault::getInstance()->setIntegerForKey(currentSizeEasy, _currentSize);
            UserDefault::getInstance()->flush();
        } else if (level == 2) {
            UserDefault::getInstance()->setIntegerForKey(currentSizeNormal, _currentSize);
            UserDefault::getInstance()->flush();
        } else if (level == 3) {
            UserDefault::getInstance()->setIntegerForKey(currentSizeHard, _currentSize);
            UserDefault::getInstance()->flush();
        } else {
            UserDefault::getInstance()->setIntegerForKey(currentSizeInsane, _currentSize);
            UserDefault::getInstance()->flush();
        }
        numberRetry++;
        UserDefault::getInstance()->setIntegerForKey(numRetry, numberRetry);
        UserDefault::getInstance()->flush();
        UserDefault::getInstance()->setIntegerForKey(santaHat, numSantaHat - needHat);
        Director::getInstance()->replaceScene(TransitionFade::create(0.5, GameScene::create(_numRow, _numCol)));
    }
}
void GameFinishLayer::setData(int numRow, int numCol, float timmer, int score) {
    _numRow = numRow;
    _numCol = numCol;
    _timmer = timmer;
    _score = score;
}
void GameFinishLayer::runAni() {
    _mainChild->runAction(FadeIn::create(0.3));
    _mainChild->runAction(Sequence::create(ScaleTo::create(0.3, 1, 1), CallFunc::create([&] {
        int randNum = rand() % 3;
        if (randNum == 2) {
            CallBridge::showInterstitial();
        }
    }), NULL));
}
void GameFinishLayer::backHome(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    Director::getInstance()->replaceScene(TransitionFade::create(0.6, IntroScene::createScene()));
}
void GameFinishLayer::updateSantaHat() {
    Label *lb = static_cast<Label*>(_mainChild->getChildByTag(99));
    lb->runAction(Sequence::create(ScaleTo::create(0.1, 1.5),DelayTime::create(0.025), CallFunc::create([&] {
        Label *lb = static_cast<Label*>(_mainChild->getChildByTag(99));
        lb->setString(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0)    )->getCString());
        lb->runAction(ScaleTo::create(0.1, 1));
    }), NULL));

}
void GameFinishLayer::shareFacebook(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::firstShareFacebook();
}
void GameFinishLayer::menuLeaderboardCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callShowLeaderboard();
}

void GameFinishLayer::menuRateCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callAppRate();
}