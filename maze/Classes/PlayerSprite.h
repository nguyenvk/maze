//
//  PlayerSprite.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/9/14.
//
//

#ifndef __maze__PlayerSprite__
#define __maze__PlayerSprite__

#include <stdio.h>
#include "cocos2d.h"
#include "Model.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;
class PlayerSpriteProtocol {
public:
    virtual void winGame() = 0;
    virtual void changePlayerMovingStatus(bool flag) = 0;
};
class PlayerSprite: public Sprite {
public:
    CREATE_FUNC(PlayerSprite);
    virtual bool init();
    Sprite *_santaUp,*_santaDown,*_santaRight,*_santaLeft;
    void initMaze(__Array *maze, int numRow, int numCol);
    __Array *_maze;
    float _cellWidth,_cellHeight,_radio;
    float getSize();
    Point _startPoint,_endPoint,_currentPoint;
    void setCampain(Point startPoint, Point endPoint);
    int _numRow = 10, _numCol = 10;
    void moveLeft();
    void moveUp();
    void moveRight();
    void moveDown();
    void checkFinishMaze();
    PlayerSpriteProtocol *delegate;
};

#endif /* defined(__maze__PlayerSprite__) */
