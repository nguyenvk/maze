//
//  IntroScene.cpp
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/20/14.
//
//

#include "IntroScene.h"
//#include "TutorialLayer.h"
#include "GameScene.h"
//#include "CallBridge.h"
#include "AppDelegate.h"


USING_NS_CC;

Scene* IntroScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = IntroScene::create();
    layer->setName("IntroScene");
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool IntroScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    _winSize = Director::getInstance()->getVisibleSize();
    
    LayerColor *bg = LayerColor::create(Color4B(230, 230, 230, 255), _winSize.width, _winSize.height);
    bg->setPosition(0,0);
    this->addChild(bg);
    
    Sprite *sprite;
    sprite = Sprite::create("icon.png");
    sprite->setPosition(_winSize.width/2, _winSize.height * 1.9 / 3);
    this->addChild(sprite);
    
    if (UserDefault::getInstance()->getIntegerForKey(medalEasy, 0)) {
        sprite = Sprite::create("medalEasy.png");
        sprite->setScale(0.6);
        sprite->setPosition(_winSize.width * 9 / 10, _winSize.height * 9.4/10);
        this->addChild(sprite);
    }
    if (UserDefault::getInstance()->getIntegerForKey(medalNormal, 0)) {
        sprite = Sprite::create("medalNormal.png");
        sprite->setScale(0.6);
        sprite->setPosition(_winSize.width *9 / 10, _winSize.height *8.8 / 10);
        this->addChild(sprite);
    }
    if (UserDefault::getInstance()->getIntegerForKey(medalHard, 0)) {
        sprite = Sprite::create("medalHard.png");
        sprite->setScale(0.6);
        sprite->setPosition(_winSize.width * 9 / 10, _winSize.height * 8.2 / 10);
        this->addChild(sprite);
    }
    if (UserDefault::getInstance()->getIntegerForKey(medalInsane, 0)) {
        sprite = Sprite::create("medalInsane.png");
        sprite->setScale(0.6);
        sprite->setPosition(_winSize.width * 9 / 10, _winSize.height * 7.6 / 10);
        this->addChild(sprite);
    }
    
    sprite = Sprite::create("santas_hat.png");
    sprite->setPosition(_winSize.width * 1.3 / 3, _winSize.height * 9.5 / 10);
    this->addChild(sprite);
    Label *labelSantaHat = Label::createWithSystemFont(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString(), "Roboto-Light", 48);
    labelSantaHat->setColor(Color3B::BLACK);
    labelSantaHat->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
    labelSantaHat->setPosition(sprite->getPositionX() + sprite->getContentSize().width /2  + 10, sprite->getPositionY() + 4);
    this->addChild(labelSantaHat,1,99);
    
    this->createMenu();
    
    _selectLevelLayer = SelectLevelLayer::create();
    _selectLevelLayer->setPosition(0,0);
    _selectLevelLayer->delegate = this;
    this->addChild(_selectLevelLayer,10);
    return true;
}
void IntroScene::startGameEasy(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    UserDefault::getInstance()->setIntegerForKey(numRetry, 0);
    if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 1) {
        int size = UserDefault::getInstance()->getIntegerForKey(currentSizeEasy, 0);
        if (size == 0) {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(5,5)));
        } else {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(size,size)));
        }
    } else if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 2) {
        int size = UserDefault::getInstance()->getIntegerForKey(currentSizeNormal, 0);
        if (size == 0) {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(9,9)));
        } else {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(size,size)));
        }
    } else if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 3) {
        int size = UserDefault::getInstance()->getIntegerForKey(currentSizeHard, 0);
        if (size == 0) {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(13,13)));
        } else {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(size,size)));
        }
    } else {
        int size = UserDefault::getInstance()->getIntegerForKey(currentSizeInsane, 0);
        if (size == 0) {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(17,17)));
        } else {
            Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(size,size)));
        }
    }
}
void IntroScene::choiseLevel(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    _selectLevelLayer->aniShow();
}

void IntroScene::menuLeaderboardCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callShowLeaderboard();
}

void IntroScene::menuRateCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callAppRate();
}
void IntroScene::menuRemoveAdsCallback(cocos2d::Ref *pSender)
{
}
void IntroScene::showIAP(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    IAPLayer *iap = IAPLayer::create();
    iap->setPosition(0,0);
    iap->delegate = this;
    this->addChild(iap,9999,9999);
    iap->runAniShow();
}
void IntroScene::moreGame(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::moreGame();
}
void IntroScene::shareFacebook(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::firstShareFacebook();
}
void IntroScene::updateSelectLevel() {
    this->getChildByName("mainMenu")->removeFromParentAndCleanup(true);
    this->createMenu();
}
void IntroScene::createMenu() {
    MenuItemLabel *leaderMenu = MenuItemLabel::create(Sprite::create("scoresButton.png"), CC_CALLBACK_1(IntroScene::menuLeaderboardCallback, this));
    leaderMenu->setPosition(_winSize.width * 0.92 / 3, _winSize.height * 1.8 / 5);
    
    MenuItemLabel *startMenu = MenuItemLabel::create(Sprite::create("playButton.png"), CC_CALLBACK_1(IntroScene::startGameEasy, this));
    startMenu->setPosition(_winSize.width * 2.08 / 3, _winSize.height * 1.8 / 5);
    Label *lb;
    if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 1) {
        lb = Label::createWithSystemFont("Ease", "Roboto-Regular", 18);
    } else if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 2) {
        lb = Label::createWithSystemFont("Normal", "Roboto-Regular", 18);
    } else if (UserDefault::getInstance()->getIntegerForKey(selectLevel, 1) == 3) {
        lb = Label::createWithSystemFont("Hard", "Roboto-Regular", 18);
    } else {
        lb = Label::createWithSystemFont("Insane", "Roboto-Regular", 18);
    }
    lb->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
    lb->setPosition(startMenu->getContentSize().width /2 , startMenu->getContentSize().height * 1 / 4);
    lb->setColor(Color3B::BLACK);
    startMenu->addChild(lb);
    
    
    MenuItemLabel *shareMenu = MenuItemLabel::create(Sprite::create("shareButton.png"), CC_CALLBACK_1(IntroScene::shareFacebook, this));
    shareMenu->setPosition(_winSize.width * 1 / 3, _winSize.height * 1.15 / 5);
    shareMenu->setScale(0.8);
    
    MenuItemLabel *rateMenu = MenuItemLabel::create(Sprite::create("rateButton.png"), CC_CALLBACK_1(IntroScene::menuRateCallback, this));
    rateMenu->setPosition(_winSize.width * 2 / 3, _winSize.height * 1.15 / 5);
    rateMenu->setScale(0.8);
    
    lb = Label::createWithSystemFont("SELECT LEVEL", "Roboto-Thin", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *selectLevelMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IntroScene::choiseLevel, this));
    selectLevelMenu->setPosition(_winSize.width * 1 / 6, _winSize.height * 9.5 / 10);
    
    lb = Label::createWithSystemFont("", fontAs, 42);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *getSantaHat = MenuItemLabel::create(lb, CC_CALLBACK_1(IntroScene::showIAP, this));
    Label *lbSantaHat = static_cast<Label*>(this->getChildByTag(99));
    getSantaHat->setPosition(lbSantaHat->getPositionX() + lbSantaHat->getContentSize().width + lb->getContentSize().width / 2, lbSantaHat->getPositionY() - 4);
    
    if (UserDefault::getInstance()->getIntegerForKey("soundFlag", 1) == 1) {
        _volumeSprite = Sprite::create("speaker.png");
    } else {
        _volumeSprite = Sprite::create("speakerOff.png");
    }
    
    MenuItemSprite *soundMenu = MenuItemSprite::create(_volumeSprite, NULL, CC_CALLBACK_1(IntroScene::switchSoud, this));
    soundMenu->setPosition(_volumeSprite->getContentSize().width / 2, _winSize.height * 4.4 / 5);
    soundMenu->setScale(0.75);
    
    lb = Label::createWithSystemFont("MORE GAME", "Roboto-Regular", 42);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *moreGame = MenuItemLabel::create(lb, CC_CALLBACK_1(IntroScene::moreGame, this));
    moreGame->setPosition(_winSize.width / 2, _winSize.height * 0.8 / 5);
    
    Menu *menu = Menu::create(leaderMenu,startMenu,shareMenu,rateMenu, selectLevelMenu,getSantaHat,soundMenu, moreGame,  NULL);
    menu->setPosition(Point::ZERO);
    menu->setName("mainMenu");
    this->addChild(menu);
}
void IntroScene::updateSantaHat() {
    Label *lb = static_cast<Label*>(this->getChildByTag(99));
    lb->runAction(Sequence::create(ScaleTo::create(0.1, 1.5),DelayTime::create(0.025), CallFunc::create([&] {
        Label *lb = static_cast<Label*>(this->getChildByTag(99));
        lb->setString(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString());
        lb->runAction(ScaleTo::create(0.1, 1));
    }), NULL));
}
void IntroScene::switchSoud(cocos2d::Ref *pSender) {
    if (UserDefault::getInstance()->getIntegerForKey("soundFlag", 1) == 1) {
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0);
        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0);
        UserDefault::getInstance()->setIntegerForKey("soundFlag", 0);
        UserDefault::getInstance()->flush();
        _volumeSprite->setSpriteFrame(SpriteFrame::create("speakerOff.png", Rect(0, 0, 150, 150)));
    } else {
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1);
        CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.4f);
        UserDefault::getInstance()->setIntegerForKey("soundFlag", 1);
        UserDefault::getInstance()->flush();
        _volumeSprite->setSpriteFrame(SpriteFrame::create("speaker.png", Rect(0, 0, 150, 150)));
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
}
