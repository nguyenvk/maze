//
//  WingameLayer.h
//  maze
//
//  Created by Nguyen Vo Kim on 12/10/14.
//
//

#ifndef __maze__WingameLayer__
#define __maze__WingameLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "Model.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

class WingameLayer: public LayerColor {
public:
    virtual bool init();
    static WingameLayer* create();
    Size _winSize;
    LayerColor *_mainChild;
    Label *_lbAniScore;
    int _numRow,_numCol,_score,_startAddScore, _endAddScore,_totalSantaHat, _currentSantaHat;
    float _timmer, _timeAni = 1;
    bool _canGo = false;
    void runAni();
    void aniUpscore(float dt);
    void aniUpSantaHat(float dt);
    void setData(int numRow, int numCol, float timmer, int score);
    void nextLevel(cocos2d::Ref* pSender);
    void checkFinishLevel();
    __String *_fileCapturePath = NULL;
    void setFileCapture(__String *str);
    void shareFaceBook(cocos2d::Ref* pSender);
    void menuRateCallback(cocos2d::Ref* pSender);
    void menuLeaderboardCallback(cocos2d::Ref* pSender);
    void backHome(cocos2d::Ref *pSender);
};
#endif /* defined(__maze__WingameLayer__) */
