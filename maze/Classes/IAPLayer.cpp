//
//  IAPLayer.cpp
//  RollingBall
//
//  Created by Nguyen Vo Kim on 11/26/14.
//
//

#include "IAPLayer.h"
#include "AppDelegate.h"

IAPLayer* IAPLayer::create()
{
    IAPLayer *pRet = new IAPLayer();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool IAPLayer::init()
{
    _winSize = Director::getInstance()->getVisibleSize();
    if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 180), _winSize.width, _winSize.height))
    {
        return false;
    }
    this->setName("IAPLayer");
    MenuItemSprite *mnSP = MenuItemSprite::create(Sprite::create("blank.png", Rect(0, 0, 640, 1024)), NULL);
    mnSP->setPosition(_winSize.width/2, _winSize.height/2);
    mnSP->setOpacity(0);
    Menu *mn = Menu::create(mnSP, NULL);
    mn->setPosition(0,0);
    this->addChild(mn);
    
    _bg = Sprite::create("bgSelectLevel.png");
    
    _bg->setPosition(_winSize.width / 2, - _winSize.height / 2);
    this->addChild(_bg);
    //this->createMenu();
    return true;
}
void IAPLayer::runAniShow() {
    _bg->runAction(Sequence::create(EaseBackOut::create(MoveTo::create(0.5, Point(_winSize.width / 2, _winSize.height /2))), DelayTime::create(0.2), CallFunc::create([&] {
        this->createMenu();
    }) , NULL));
}
void IAPLayer::buyItem(cocos2d::Ref* pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    MenuItemLabel *mlb = static_cast<MenuItemLabel*>(pSender);
    CallBridge::getListIAPItem(mlb->getTag());
}
void IAPLayer::closeIAP(cocos2d::Ref* pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    _bg->runAction(Sequence::create(EaseBackIn::create(MoveTo::create(0.5, Point(_winSize.width /2, - _winSize.height / 2))),DelayTime::create(0.3),CallFunc::create([&](){
        if (delegate) {
            delegate->updateSantaHat();
        }
        this->removeFromParentAndCleanup(true);
    }),NULL));
    //this->removeFromParentAndCleanup(true);
}
void IAPLayer::checkRemoveMenuAds() {
    this->removeAllChildren();
    this->createMenu();
}
void IAPLayer::createMenu() {
    bool isHideAdsFlag = UserDefault::getInstance()->getBoolForKey("isHideAds", false);
    float firstY = 5,nextY = 1;
    if (!isHideAdsFlag) {
        firstY = 5.2;
        nextY = 0.9;
    }
    Size bgSize = _bg->getContentSize();
    Label *lb = Label::createWithSystemFont("CLOSE", "Roboto-Light", 32);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *close = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::closeIAP, this));
    close->setPosition(bgSize.width *1 / 4, bgSize.height * 0.8 / 6);
    
    lb = Label::createWithSystemFont("Restone Purchase", "Roboto-Light", 32);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *resMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::restonePurchase, this));
    resMenu->setPosition(bgSize.width *2.6 / 4, bgSize.height * 0.8 / 6);
    
    lb = Label::createWithSystemFont("BUY 200 SANTA HATS FOR 0.99$", "Roboto-Light", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *easeMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::buyItem, this));
    easeMenu->setPosition(bgSize.width /2, bgSize.height * (firstY) / 6);
    easeMenu->setTag(1);
    
    firstY = firstY - nextY;
    lb = Label::createWithSystemFont("BUY 1100 SANTA HATS FOR 4.99$", "Roboto-Light", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *normalMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::buyItem, this));
    normalMenu->setPosition(bgSize.width /2, bgSize.height * firstY / 6);
    normalMenu->setTag(0);
    
    firstY = firstY - nextY;
    lb = Label::createWithSystemFont("BUY 2300 SANTA HATS FOR 9.99$", "Roboto-Light", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *hardMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::buyItem, this));
    hardMenu->setPosition(bgSize.width /2, bgSize.height * firstY / 6);
    hardMenu->setTag(2);
    
    firstY = firstY - nextY;
    lb = Label::createWithSystemFont("BUY 4700 SANTA HATS FOR 19.99$ (BEST)", "Roboto-Light", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *insaneMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::buyItem, this));
    insaneMenu->setPosition(bgSize.width /2, bgSize.height * firstY / 6);
    insaneMenu->setTag(3);
    
    firstY = firstY - nextY;
    lb = Label::createWithSystemFont("Remove Ads", "Roboto-Light", 24);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *removeAdsMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(IAPLayer::buyItem, this));
    removeAdsMenu->setPosition(bgSize.width /2, bgSize.height * firstY / 6);
    removeAdsMenu->setTag(4);
    
    Menu *menu;
    if (isHideAdsFlag) {
        menu = Menu::create(close, easeMenu, normalMenu, hardMenu, insaneMenu, resMenu, NULL);
    } else {
        menu = Menu::create(close, easeMenu, normalMenu, hardMenu, insaneMenu,removeAdsMenu, resMenu, NULL);
    }
    menu->setPosition(0,0);
    menu->setOpacity(0);
    _bg->addChild(menu);
    menu->runAction(FadeIn::create(0.3));
}
void IAPLayer::restonePurchase(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::restonePurchase();
}