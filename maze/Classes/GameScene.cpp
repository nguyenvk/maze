//
//  GameScene.cpp
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/19/14.
//
//

#include "GameScene.h"
#include "cocos2d.h"
#include "Model.h"
GameScene::GameScene(){}

GameScene::~GameScene(){}
GameScene* GameScene::create(int numRow, int numCol)
{
    GameScene *pRet = new GameScene();
    if (pRet && pRet->init(numRow, numCol))
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}
bool GameScene::init(int numRow, int numCol){
	if(Scene::initWithPhysics()){
		//this->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
        Size winsize = Director::getInstance()->getVisibleSize();
        this->getPhysicsWorld()->setGravity(Vect(0, -3000));
        auto gameLayer = GameLayer::create(numRow, numCol);
        gameLayer->setName("gameLayer");
        if(gameLayer) {
            this->addChild(gameLayer);
        }
        auto elgdeBody = PhysicsBody::createEdgeBox(Size(winsize.width * 10, winsize.height), PhysicsMaterial(0.0f ,0.5f ,0.5f), 3);
        elgdeBody->setCategoryBitmask(-1);
        elgdeBody->setCollisionBitmask(-1);
        elgdeBody->setContactTestBitmask(-1);
        
        auto elgdeNode = Node::create();
        elgdeNode->setPosition(winsize.width/2, winsize.height/2);
        elgdeNode->setPhysicsBody(elgdeBody);
        this->addChild(elgdeNode);
        return true;
	} else {
		return false;
	}
}