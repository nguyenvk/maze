//
//  GameLayer.cpp
//  SwingCopter
//
//  Created by Le Thanh Hai on 8/20/14.
//
//

#include "GameLayer.h"

GameLayer::GameLayer(){}

GameLayer::~GameLayer(){}

GameLayer* GameLayer::create(int numRow, int numCol)
{
    GameLayer *pRet = new GameLayer();
    if (pRet && pRet->init(numRow, numCol))
    {
        pRet->autorelease();
        return pRet; 
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool GameLayer::init(int numRow, int numCol)
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init())
    {
        return false;
    }
    _winSize = Director::getInstance()->getVisibleSize();
    int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    if (level == 1) {
        _score = UserDefault::getInstance()->getIntegerForKey(scoreEasy, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeEasy, numRow);
        UserDefault::getInstance()->flush();
    } else if (level == 2) {
        _score = UserDefault::getInstance()->getIntegerForKey(scoreNormal, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeNormal, numRow);
        UserDefault::getInstance()->flush();
    } else if (level == 3) {
        _score = UserDefault::getInstance()->getIntegerForKey(scoreHard, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeHard, numRow);
        UserDefault::getInstance()->flush();
    } else {
        _score = UserDefault::getInstance()->getIntegerForKey(scoreInsane, 0);
        UserDefault::getInstance()->setIntegerForKey(currentSizeInsane, numRow);
        UserDefault::getInstance()->flush();
    }
    _numRow = numRow;
    _numCol = numCol;
    // add contact listener
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(GameLayer::onContactBegin, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(contactListener, this);
    // set touch to screen
    auto listener = EventListenerTouchAllAtOnce::create();
    listener->onTouchesBegan = CC_CALLBACK_2(GameLayer::onTouchesBegan, this);
    listener->onTouchesEnded = CC_CALLBACK_2(GameLayer::onTouchesEnded, this);
    listener->onTouchesMoved = CC_CALLBACK_2(GameLayer::onTouchesMoved, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
    
    LayerColor *bg = LayerColor::create(Color4B::WHITE, _winSize.width, _winSize.height);
    bg->setPosition(0,0);
    this->addChild(bg);
    
    //tam thoi thoi gian se la so o
    _timmer = Model::calMazeTime(_numRow, _numCol, UserDefault::getInstance()->getIntegerForKey(selectLevel, 1));
    
    _maze = Model::gentMazeData(_numRow, _numCol);
    _maze->retain();
    
    __Dictionary *dict;
    for (int i = 0; i < _numRow; i++) {
        for (int j = 0; j < _numCol; j++) {
            dict =  Model::getMazeData(_maze, i, j, _numRow, _numCol);
            CCLOG("Top: %d,Right: %d,Down: %d,Left: %d", static_cast<__Integer*>(dict->objectForKey(TOP))->getValue(),static_cast<__Integer*>(dict->objectForKey(RIGHT))->getValue(),static_cast<__Integer*>(dict->objectForKey(DOWN))->getValue(),static_cast<__Integer*>(dict->objectForKey(LEFT))->getValue());
        }
    }
    Point startPoint = Point(0,0);
    Point endPoint = Point(_numRow - 1, _numCol - 1);
    
    _mazeLayer = DrawMazeLayer::create(_maze, _numRow, _numCol);
    _mazeLayer->setPosition(10, _winSize.height /2 - _mazeLayer->getContentSize().height / 2);
    _mazeLayer->setMazeCampain(startPoint, endPoint);
    _mazeLayer->delegate = this;
    _mazeLayer->addReindder();
    _mazeLayer->addSantaHat();
    this->addChild(_mazeLayer);
    _lc = LayerColor::create(Color4B(255, 255, 255, 200), _mazeLayer->getContentSize().width + 20, _mazeLayer->getContentSize().height + 20);
    _lc->setPosition(_mazeLayer->getPositionX() - 10, _mazeLayer->getPositionY() - 10);
    this->addChild(_lc);
    
    int gameLevel = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    int numHat;
    if (gameLevel == 1) {
        numHat = 4;
    } else if (gameLevel == 2) {
        numHat = 8;
    } else if (gameLevel == 3) {
        numHat = 16;
    } else {
        numHat = 32;
    }
    Label *lb = Label::createWithSystemFont(__String::createWithFormat("HINT (USE %d SANTA HAT)", numHat)->getCString() ,"Roboto-Regular", 32);
    lb->setColor(Color3B::BLACK);
    MenuItem *hintMenu = MenuItemLabel::create(lb, CC_CALLBACK_1(GameLayer::hint, this));
    hintMenu->setPosition(_winSize.width / 2, _mazeLayer->getPositionY() - 40);
    Menu *menu = Menu::create(hintMenu, NULL);
    menu->setPosition(0,0);
    this->addChild(menu);
    
    //init player, mac dinh dang la 0-0, tuong lai ngau nhien diem start va end
    _player = PlayerSprite::create();
    _player->initMaze(_maze, _numRow, _numCol);
    _player->setCampain(startPoint, endPoint);
    _player->delegate = this;
    
    _mazeLayer->addChild(_player,10,10);
    
    Sprite *sprite = Sprite::create("santas_hat.png");
    sprite->setPosition(sprite->getContentSize().width / 2 + 20, _winSize.height * 9.5 / 10);
    this->addChild(sprite);
    Label *labelSantaHat = Label::createWithSystemFont(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString(), "Roboto-Light", 48);
    labelSantaHat->setColor(Color3B::BLACK);
    labelSantaHat->setName("labelSantaHat");
    labelSantaHat->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
    labelSantaHat->setPosition(sprite->getPositionX() + sprite->getContentSize().width /2  + 10, sprite->getPositionY());
    this->addChild(labelSantaHat,1,99);
    
    _timeLabel = Label::createWithSystemFont(__String::createWithFormat("%d",int(floor(_timmer)))->getCString(), "Roboto-Light", 64);
    _timeLabel->setPosition(_winSize.width/2, _winSize.height * 9.4 / 10);
    _timeLabel->setColor(Color3B::BLACK);
    this->addChild(_timeLabel);
    
    _aniLabel = Label::createWithSystemFont("3", "Roboto-Light", 96);
    _aniLabel->setPosition(_winSize.width /2 , _winSize.height / 2);
    _aniLabel->setColor(Color3B::BLACK);
    this->addChild(_aniLabel);
    this->aniGameStart();
    this->scheduleUpdate();
    
    return true;
}

void GameLayer::update(float dt)
{
    if (_gameStatus) {
        _timmer = _timmer - dt;
        _timeLabel->setString(__String::createWithFormat("%d",int(ceil(_timmer)))->getCString());
        if (_timmer < 0) {
            _gameStatus = false;
            this->lostGame();
        }
    }
}
void GameLayer::onTouchesBegan(const std::vector<Touch*>& touches, Event *unused_event)
{
    if (!_gameStatus || _playerMovingFlag) return;
    _touchStart = touches.at(0)->getLocationInView();
    _touchStart = Director::getInstance()->convertToGL(_touchStart);
}

void GameLayer::onTouchesEnded(const std::vector<Touch*>& touches, Event *unused_event)
{
    if (!_gameStatus || _playerMovingFlag) {
        return;
    }
    _touchEnd = touches.at(0)->getLocationInView();
    _touchEnd = Director::getInstance()->convertToGL(_touchEnd);
    float angle = atan2(_touchEnd.y - _touchStart.y, _touchEnd.x - _touchStart.x) * 180 / M_PI;
    CCLOG("%f", angle);
    if (angle >= -135 && angle < -45) {
        _player->moveDown();
    } else if (angle >= -45 && angle < 45) {
        _player->moveRight();
    } else if (angle >= 45 && angle < 135) {
        _player->moveUp();
    } else {
        _player->moveLeft();
    }
//    for ( auto &item: touches )
//    {
////        float touchX = s_map.at(item->getID())->getLocation().x;
////        if (touchX < _winSize.width/2) {
////            _player->_vectorLeft = 0;
////        } else {
////            _player->_vectorRight = 0;
////        }
//        s_map.erase(item->getID());
//    }
}

void GameLayer::onTouchesMoved(const std::vector<Touch*>& touches, Event *unused_event)
{
//    for ( auto &item: touches )
//    {
//        float touchX = s_map.at(item->getID())->getLocation().x;
//        if (touchX < _winSize.width/2) {
//            _player->_vectorLeft = speedHori;
//        } else {
//            _player->_vectorRight = speedHori;
//        }
//    }
}

bool GameLayer::onContactBegin(PhysicsContact& contact) {
    
    return true;
}
void GameLayer::shakeScreen(float dt) {
    if (_timeShake > 0) {
        float x = (arc4random() % 20) - 2.5f; //Generate a random x coordinate
        float y = (arc4random() % 20
                   ) - 2.5f; //Do the same for the y coordinate
        this->setPosition(x,y); //Offset the layer's position by x and y
        _timeShake = _timeShake - dt; //Subtract one from the length
    } else {
        this->setPosition(0,0);
        this->unschedule(schedule_selector(GameLayer::shakeScreen));
    }
}
void GameLayer::movePlayer(cocos2d::Ref *pSender) {
    if (!_gameStatus) {
        return;
    }
    Label *lb = static_cast<Label*>(pSender);
    if (lb->getTag() == 1) {
        _player->moveUp();
    } else if (lb->getTag() == 2) {
        _player->moveRight();
    }
    else if (lb->getTag() == 3) {
        _player->moveDown();
    }
    else if (lb->getTag() == 4) {
        _player->moveLeft();
    }
}
void GameLayer::aniGameStart() {
    
    _aniLabel->runAction(Sequence::create(DelayTime::create(1),ScaleTo::create(0.2, 1.5), CallFunc::create([&] {
        _aniLabel->setString("2");
        _aniLabel->runAction(Sequence::create(ScaleTo::create(0.2, 1), DelayTime::create(0.6), ScaleTo::create(0.2, 1.5), CallFunc::create([&] {
            _aniLabel->setString("1");
            _aniLabel->runAction(Sequence::create(ScaleTo::create(0.2, 1), DelayTime::create(1), CallFunc::create([&] {
                _lc->runAction(Sequence::create(FadeOut::create(0.3), CallFunc::create([&] {
                    _lc->removeFromParentAndCleanup(true);
                    _lc = NULL;
                }), NULL));
                _aniLabel->setString("GO!");
                _aniLabel->runAction(FadeOut::create(0.5));
                _aniLabel->runAction(Sequence::create(ScaleTo::create(0.5, 5), CallFunc::create([&] {
                    _aniLabel->removeFromParentAndCleanup(true);
                    _aniLabel = NULL;
                    _gameStatus = true;
                    if (_mazeLayer->_resultPath) {
                        //_mazeLayer->autoMove(0);
                    }
                }), NULL));
            }), NULL));
        }), NULL));
    }), NULL));
}
void GameLayer::winGame() {
    _gameStatus = false;
    _mazeLayer->addWinReindder();
    _mazeLayer->drawSloveMaze(0);
}
void GameLayer::showWinLayer() {
    utils::captureScreen(CC_CALLBACK_2(GameLayer::afterCapture, this), "xxx.png");
}
void GameLayer::lostGame() {
    _gameStatus = false;
    this->runAction(Sequence::create(DelayTime::create(1), CallFunc::create([&] {
        int randNum = rand() % 3;
        if (randNum == 2) {
            CallBridge::showInterstitial();
        }
        GameFinishLayer *gfl = GameFinishLayer::create();
        gfl->setData(_numRow, _numCol, _timmer, _score);
        gfl->setPosition(_winSize.width,0);
        this->addChild(gfl,1,30);
        gfl->runAction(Sequence::create(MoveTo::create(0.4, Point(0,0)),DelayTime::create(0.3) ,CallFunc::create([&] {
            static_cast<GameFinishLayer*>(this->getChildByTag(30))->runAni();
        }), NULL));
    }), NULL));
}
void GameLayer::debug(cocos2d::Ref *pSender) {
    Point sPoint = Point(0,0);
    Point ePoint = Point(_numRow - 1, _numCol - 1);
    __Dictionary *startPoint = __Dictionary::create();
    startPoint->setObject(__Integer::create(sPoint.x), "x");
    startPoint->setObject(__Integer::create(sPoint.y), "y");
    __Dictionary *endPoint = __Dictionary::create();
    endPoint->setObject(__Integer::create(ePoint.x), "x");
    endPoint->setObject(__Integer::create(ePoint.y), "y");
    _mazeLayer->runDebug(_maze, startPoint, endPoint, _numRow, _numCol);
}
void GameLayer::changePlayerMovingStatus(bool flag) {
    _playerMovingFlag = flag;
    if (flag == false) {
        //kiem tra luon xem player co an dc hat o day luon cho no tien
        int locX = _player->_currentPoint.x;
        int locY = _player->_currentPoint.y;
        for (int i = 0; i < _mazeLayer->_santahatArrSpr->count(); i++) {
            __Array *locArr = _mazeLayer->_santahatLoc.at(i);
            if (static_cast<__Integer*>(locArr->getObjectAtIndex(0))->getValue() == locX && static_cast<__Integer*>(locArr->getObjectAtIndex(1))->getValue() == locY) {
                CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("uphat.m4a");
                int numSantaHat = UserDefault::getInstance()->getIntegerForKey(santaHat, 0);
                numSantaHat++;
                UserDefault::getInstance()->setIntegerForKey(santaHat, numSantaHat);
                UserDefault::getInstance()->flush();
                Sprite *sp = static_cast<Sprite*>(_mazeLayer->_santahatArrSpr->getObjectAtIndex(i));
                sp->removeFromParentAndCleanup(true);
                _mazeLayer->_santahatArrSpr->removeObjectAtIndex(i);
                _mazeLayer->_santahatLoc.erase(i);
                this->updateSantaHat();
            }
        }
    }
}
void GameLayer::hint(cocos2d::Ref *pSender) {
    if (_gameStatus == false) {
        return;
    }
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    int numSantaHat = UserDefault::getInstance()->getIntegerForKey(santaHat, 0);
    int gameLevel = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    int numHat;
    if (gameLevel == 1) {
        numHat = 4;
    } else if (gameLevel == 2) {
        numHat = 8;
    } else if (gameLevel == 3) {
        numHat = 16;
    } else {
        numHat = 32;
    }
    if (numSantaHat < numHat) {
        _gameStatus = false;
        IAPLayer *iap = IAPLayer::create();
        iap->setPosition(0,0);
        iap->delegate = this;
        this->addChild(iap,9999,9999);
        iap->runAniShow();
    } else {
        if (_mazeLayer->drawHint()) {
            Label *lb = static_cast<Label*>(this->getChildByTag(99));
            lb->runAction(Sequence::create(ScaleTo::create(0.1, 1.5),DelayTime::create(0.025), CallFunc::create([&] {
                Label *lb = static_cast<Label*>(this->getChildByTag(99));
                int numSantaHat = UserDefault::getInstance()->getIntegerForKey(santaHat, 0);
                int gameLevel = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
                int numHat;
                if (gameLevel == 1) {
                    numHat = 4;
                } else if (gameLevel == 2) {
                    numHat = 8;
                } else if (gameLevel == 3) {
                    numHat = 16;
                } else {
                    numHat = 32;
                }
                lb->setString(__String::createWithFormat("%d", numSantaHat - numHat)->getCString());
                lb->runAction(ScaleTo::create(0.1, 1));
                UserDefault::getInstance()->setIntegerForKey(santaHat, numSantaHat - numHat);
                UserDefault::getInstance()->flush();
            }), NULL));
        };
    }
}
void GameLayer::updateSantaHat() {
    _gameStatus = true;
    Label *lb = static_cast<Label*>(this->getChildByTag(99));
    lb->runAction(Sequence::create(ScaleTo::create(0.1, 1.5),DelayTime::create(0.025), CallFunc::create([&] {
        Label *lb = static_cast<Label*>(this->getChildByTag(99));
        lb->setString(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString());
        lb->runAction(ScaleTo::create(0.1, 1));
    }), NULL));
}
void GameLayer::afterCapture(bool flag, const std::string& fileName) {
    _fileCapturePath = __String::create(fileName);
    _fileCapturePath->retain();
    CCLOG("%s", _fileCapturePath->getCString());
    this->runAction(Sequence::create(DelayTime::create(1), CallFunc::create([&] {
        int randNum = rand() % 3;
        if (randNum == 2) {
            CallBridge::showInterstitial();
        }
        WingameLayer *wgl = WingameLayer::create();
        wgl->setFileCapture(_fileCapturePath);
        wgl->setPosition(_winSize.width, 0);
        wgl->setData(_numRow, _numCol, _timmer, _score);
        this->addChild(wgl,1,30);
        wgl->runAction(Sequence::create(MoveTo::create(0.4, Point(0,0)),DelayTime::create(0.3) ,CallFunc::create([&] {
            static_cast<WingameLayer*>(this->getChildByTag(30))->runAni();
        }), NULL));
    }), NULL));
}