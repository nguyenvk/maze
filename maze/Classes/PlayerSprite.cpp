//
//  PlayerSprite.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/9/14.
//
//

#include "PlayerSprite.h"

bool PlayerSprite::init()
{
    if (!Sprite::create()) {
        return false;
    }
    this->setName("player");
    this->initWithFile("santaDown.png");
    //this->setOpacity(0);
    
    _santaUp = Sprite::create("santaUp.png");
    _santaUp->setPosition(this->getContentSize().width/2,this->getContentSize().height/2);
    this->addChild(_santaUp);
    _santaUp->setOpacity(0);
    
    _santaLeft = Sprite::create("santaLeft.png");
    _santaLeft->setPosition(this->getContentSize().width/2,this->getContentSize().height/2);
    this->addChild(_santaLeft);
    _santaLeft->setOpacity(0);
    
    _santaDown = Sprite::create("santaDown.png");
    _santaDown->setPosition(this->getContentSize().width/2,this->getContentSize().height/2);
    this->addChild(_santaDown);
    _santaDown->setOpacity(0);
    
    _santaRight = Sprite::create("santaRight.png");
    _santaRight->setPosition(this->getContentSize().width/2,this->getContentSize().height/2);
    this->addChild(_santaRight);
    _santaRight->setOpacity(0);
    
    return true;
}
void PlayerSprite::initMaze(__Array *maze, int numRow, int numCol) {
    _maze = maze;
    _numRow = numRow;
    _numCol = numCol;
    float width = 620;
    float height = width * numCol / numRow;
    _cellWidth = width / numRow;
    _cellHeight = height / numCol;
    _radio = _cellWidth / this->getContentSize().height;
    this->setScale(_radio);
}
float PlayerSprite::getSize() {
    return _radio * this->getContentSize().height;
}
void PlayerSprite::setCampain(Point startPoint, Point endPoint) {
    _startPoint = startPoint;
    _currentPoint = startPoint;
    _endPoint = endPoint;
    float size = this->getSize();
    this->setPosition(_cellWidth * startPoint.x + size/2, _cellHeight * startPoint.y + size / 2);
}
void PlayerSprite::moveUp() {
    delegate->changePlayerMovingStatus(true);
//    _santaUp->setOpacity(255);
//    _santaRight->setOpacity(0);
//    _santaDown->setOpacity(0);
//    _santaLeft->setOpacity(0);
    __Dictionary *mazeCellData = Model::getMazeData(_maze, _currentPoint.x, _currentPoint.y, _numRow, _numCol);
    if (static_cast<__Integer*>(mazeCellData->objectForKey(TOP))->getValue() == 0) {
        //can move
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("move.m4a");
        _currentPoint.y = _currentPoint.y + 1;
        Vector<SpriteFrame*> aniFame(3);
        aniFame.pushBack(SpriteFrame::create("santa_03.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_01.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_03.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_02.png", Rect(0, 0, 70, 98)));
        this->runAction(Animate::create(Animation::createWithSpriteFrames(aniFame, 0.05)));
        MoveTo *mt = MoveTo::create(0.15, Point(_cellWidth * _currentPoint.x + this->getSize() / 2, _cellHeight * _currentPoint.y + this->getSize() / 2));
        this->runAction(Sequence::create(mt, CallFunc::create([&] {
            delegate->changePlayerMovingStatus(false);
            this->checkFinishMaze();
        }), NULL));
    } else {
        //not move
        delegate->changePlayerMovingStatus(false);
    }
}
void PlayerSprite::moveRight() {
    delegate->changePlayerMovingStatus(true);
//    _santaUp->setOpacity(0);
//    _santaRight->setOpacity(255);
//    _santaDown->setOpacity(0);
//    _santaLeft->setOpacity(0);
    __Dictionary *mazeCellData = Model::getMazeData(_maze, _currentPoint.x, _currentPoint.y, _numRow, _numCol);
    if (static_cast<__Integer*>(mazeCellData->objectForKey(RIGHT))->getValue() == 0) {
        //can move
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("move.m4a");
        _currentPoint.x = _currentPoint.x + 1;
        Vector<SpriteFrame*> aniFame(4);
        aniFame.pushBack(SpriteFrame::create("santa_06.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_04.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_06.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_05.png", Rect(0, 0, 70, 98)));
        this->runAction(Animate::create(Animation::createWithSpriteFrames(aniFame, 0.05)));
        MoveTo *mt = MoveTo::create(0.15, Point(_cellWidth * _currentPoint.x + this->getSize() / 2, _cellHeight * _currentPoint.y + this->getSize() / 2));
        this->runAction(Sequence::create(mt, CallFunc::create([&] {
            delegate->changePlayerMovingStatus(false);
            this->checkFinishMaze();
        }), NULL));
    } else {
        //not move
        delegate->changePlayerMovingStatus(false);
    }
}
void PlayerSprite::moveDown() {
    delegate->changePlayerMovingStatus(true);
//    _santaUp->setOpacity(0);
//    _santaRight->setOpacity(0);
//    _santaDown->setOpacity(255);
//    _santaLeft->setOpacity(0);
    __Dictionary *mazeCellData = Model::getMazeData(_maze, _currentPoint.x, _currentPoint.y, _numRow, _numCol);
    if (static_cast<__Integer*>(mazeCellData->objectForKey(DOWN))->getValue() == 0) {
        //can move
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("move.m4a");
        _currentPoint.y = _currentPoint.y -1;
        Vector<SpriteFrame*> aniFame(4);
        aniFame.pushBack(SpriteFrame::create("santa_09.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_07.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_09.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_08.png", Rect(0, 0, 70, 98)));
        this->runAction(Animate::create(Animation::createWithSpriteFrames(aniFame, 0.05)));
        MoveTo *mt = MoveTo::create(0.15, Point(_cellWidth * _currentPoint.x + this->getSize() / 2, _cellHeight * _currentPoint.y + this->getSize() / 2));
        this->runAction(Sequence::create(mt, CallFunc::create([&] {
            delegate->changePlayerMovingStatus(false);
            this->checkFinishMaze();
        }), NULL));
    } else {
        //not move
        delegate->changePlayerMovingStatus(false);
    }
}
void PlayerSprite::moveLeft() {
    delegate->changePlayerMovingStatus(true);
//    _santaUp->setOpacity(0);
//    _santaRight->setOpacity(0);
//    _santaDown->setOpacity(0);
//    _santaLeft->setOpacity(255);
    __Dictionary *mazeCellData = Model::getMazeData(_maze, _currentPoint.x, _currentPoint.y, _numRow, _numCol);
    if (static_cast<__Integer*>(mazeCellData->objectForKey(LEFT))->getValue() == 0) {
        //can move
        CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("move.m4a");
        _currentPoint.x = _currentPoint.x - 1;
        Vector<SpriteFrame*> aniFame(4);
        aniFame.pushBack(SpriteFrame::create("santa_12.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_10.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_12.png", Rect(0, 0, 70, 98)));
        aniFame.pushBack(SpriteFrame::create("santa_11.png", Rect(0, 0, 70, 98)));
        this->runAction(Animate::create(Animation::createWithSpriteFrames(aniFame, 0.05)));
        MoveTo *mt = MoveTo::create(0.15, Point(_cellWidth * _currentPoint.x + this->getSize() / 2, _cellHeight * _currentPoint.y + this->getSize() / 2));
        this->runAction(Sequence::create(mt, CallFunc::create([&] {
            delegate->changePlayerMovingStatus(false);
            this->checkFinishMaze();
        }), NULL));
    } else {
        //not move
        delegate->changePlayerMovingStatus(false);
    }
}
void PlayerSprite::checkFinishMaze() {
    if (_currentPoint.x == _endPoint.x && _currentPoint.y == _endPoint.y) {
        if (delegate) {
            delegate->winGame();
        }
    }
}