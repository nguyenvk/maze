#include "AppDelegate.h"
#include "IntroScene.h"
#include "GameLayer.h"
#include "IAPLayer.h"
#include "GameFinishLayer.h"

USING_NS_CC;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
        glview = GLViewImpl::create("My Game");
        director->setOpenGLView(glview);
    }
    float height = director->getWinSize().height;
    //glview->setDesignResolutionSize(640, 1136, ResolutionPolicy::FIXED_WIDTH);
    if (height > 2000) {
        glview->setDesignResolutionSize(640, 960, ResolutionPolicy::SHOW_ALL);
    } else {
        glview->setDesignResolutionSize(640, 1136, ResolutionPolicy::FIXED_WIDTH);
    }
    // turn on display FPS
    //director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0 / 60);
    //UserDefault::getInstance()->setIntegerForKey(santaHat, 100);

    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("animation.plist");
    CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(UserDefault::getInstance()->getIntegerForKey("soundFlag", 1));
    CocosDenshion::SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(UserDefault::getInstance()->getIntegerForKey("soundFlag", 1) ? 0.4f:0.0f);
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("winLevel1.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("uphat.m4a");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("move.m4a");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("coin.caf");
    CocosDenshion::SimpleAudioEngine::getInstance()->preloadEffect("playMenuSound.mp3");
    CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("bgmusic.mp3", true);
    // create a scene. it's an autorelease object
    auto scene = IntroScene::createScene();
    
    //UserDefault::getInstance()->setIntegerForKey(playerScore, 0);
    
    
    // run
    director->runWithScene(scene);

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
}
void AppDelegate::finishPurchase() {
    Scene *ic = Director::getInstance()->getRunningScene();
    IntroScene *introScene = dynamic_cast<IntroScene*>(ic->getChildByName("IntroScene"));
    if (introScene) {
        introScene->updateSelectLevel();
        introScene->_selectLevelLayer->aniHide(NULL);
    }
}
void AppDelegate::updateSantaHat() {
    Scene *currentS = Director::getInstance()->getRunningScene();
    GameLayer *gs = dynamic_cast<GameLayer*>(currentS->getChildByName("gameLayer"));
    if (gs) {
        IAPLayer *iap = dynamic_cast<IAPLayer*>(gs->getChildByName("IAPLayer"));
        iap->closeIAP(NULL);
    } else {
        IntroScene *introScene = dynamic_cast<IntroScene*>(currentS->getChildByName("IntroScene"));
        IAPLayer *iap = dynamic_cast<IAPLayer*>(introScene->getChildByName("IAPLayer"));
        iap->closeIAP(NULL);
    }
}
void AppDelegate::showMenuDump() {
    Scene *node = Director::getInstance()->getRunningScene();
    MenuItemLabel *mlb = MenuItemLabel::create(Label::createWithSystemFont(" ", "Arial", 1000), NULL);
    mlb->setContentSize(Director::getInstance()->getVisibleSize());
    mlb->setPosition(Director::getInstance()->getVisibleSize().width / 2, Director::getInstance()->getVisibleSize().height / 2);
    Menu *menu = Menu::create(mlb, NULL);
    menu->setName("topLayerMenu");
    menu->setPosition(0,0);
    node->addChild(menu, 999999999);
}
void AppDelegate::hideMenuDump() {
    Scene *node = Director::getInstance()->getRunningScene();
    Node *menu = node->getChildByName("topLayerMenu");
    if (menu) {
        menu->removeFromParentAndCleanup(true);
    }
}
