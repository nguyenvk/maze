//
//  WingameLayer.cpp
//  maze
//
//  Created by Nguyen Vo Kim on 12/10/14.
//
//

#include "WingameLayer.h"
WingameLayer* WingameLayer::create()
{
    WingameLayer *pRet = new WingameLayer();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = NULL;
        return NULL;
    }
}

bool WingameLayer::init()
{
    _winSize = Director::getInstance()->getVisibleSize();
    if ( !LayerColor::initWithColor(Color4B::GREEN, _winSize.width, _winSize.height))
    {
        return false;
    }
    _mainChild = LayerColor::create(Color4B(255,255,255,255), _winSize.width, _winSize.height);
    
    Label *lb = Label::createWithSystemFont("SUCCESS!", "Roboto-Bold", 128);
    lb->setColor(Color3B::GREEN);
    lb->setPosition(_winSize.width/2, _winSize.height * 3 / 4);
    _mainChild->addChild(lb);
    lb = Label::createWithSystemFont("NEXT LEVEL..", "Roboto-Light", 96);
    lb->setColor(Color3B::GREEN);
    lb->setPosition(_winSize.width/2, _winSize.height * 2.5 / 4);
    _mainChild->addChild(lb);
    
    _mainChild->setAnchorPoint(Point::ANCHOR_MIDDLE_TOP);
    _mainChild->setPosition(0, 0);
    _mainChild->setScale(0.01);
    _mainChild->setOpacity(0);
    this->addChild(_mainChild);
    
    MenuItemLabel *leaderMenu = MenuItemLabel::create(Sprite::create("scoresButton.png"), CC_CALLBACK_1(WingameLayer::menuLeaderboardCallback, this));
    leaderMenu->setPosition(_winSize.width * 0.92 / 3, _winSize.height * 1.8 / 5);
    
    MenuItemLabel *startMenu = MenuItemLabel::create(Sprite::create("playButton.png"), CC_CALLBACK_1(WingameLayer::nextLevel, this));
    startMenu->setPosition(_winSize.width * 2.08 / 3, _winSize.height * 1.8 / 5);
    lb = Label::createWithSystemFont("Next Level", "Roboto-Regular", 18);
    lb->setPosition(startMenu->getContentSize().width * 2 / 3 , startMenu->getContentSize().height * 1 / 4);
    lb->setColor(Color3B(0, 96, 142));
    startMenu->addChild(lb);
    
    MenuItemLabel *shareMenu = MenuItemLabel::create(Sprite::create("shareButton.png"), CC_CALLBACK_1(WingameLayer::shareFaceBook, this));
    shareMenu->setPosition(_winSize.width * 1 / 3, _winSize.height * 1.15 / 5);
    shareMenu->setScale(0.8);
    
    MenuItemLabel *rateMenu = MenuItemLabel::create(Sprite::create("rateButton.png"), CC_CALLBACK_1(WingameLayer::menuRateCallback, this));
    rateMenu->setPosition(_winSize.width * 2 / 3, _winSize.height * 1.15 / 5);
    rateMenu->setScale(0.8);
    
    lb = Label::createWithSystemFont("Back Home", "Roboto-Regular", 48);
    lb->setColor(Color3B::BLACK);
    MenuItemLabel *backHome = MenuItemLabel::create(lb, CC_CALLBACK_1(WingameLayer::backHome, this));
    backHome->setPosition(_winSize.width / 2, _winSize.height * 0.9 / 5);

    
    Menu *menu = Menu::create(leaderMenu,startMenu,shareMenu,rateMenu,backHome, NULL);
    menu->setPosition(Point::ZERO);
    _mainChild->addChild(menu);
    
    Sprite *sprite = Sprite::create("santas_hat.png");
    sprite->setPosition(_winSize.width * 1.3 / 3, _winSize.height * 9.5 / 10);
    _mainChild->addChild(sprite);
    Label *labelSantaHat = Label::createWithSystemFont(__String::createWithFormat("%d", UserDefault::getInstance()->getIntegerForKey(santaHat, 0))->getCString(), "Roboto-Light", 48);
    labelSantaHat->setColor(Color3B::BLACK);
    labelSantaHat->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
    labelSantaHat->setPosition(sprite->getPositionX() + sprite->getContentSize().width /2  + 10, sprite->getPositionY());
    _mainChild->addChild(labelSantaHat,1,99);
    
    return true;
}
void WingameLayer::setData(int numRow, int numCol, float timmer, int score) {
    _numRow = numRow;
    _numCol = numCol;
    _timmer = timmer;
    _score = score;
    _startAddScore = 0;
    int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    //float mazeTime = Model::calMazeTime(_numRow, _numCol, level);
    _endAddScore = timmer * 7;
    if (level == 1) {
        _endAddScore *= 0.41;
    } else if (level == 2) {
        _endAddScore *= 0.62;
    } else if (level == 3) {
        _endAddScore *= 0.87;
    } else {
        _endAddScore *= 1.11;
    }
//    CCLOG("%s", _fileCapturePath->getCString());
//    Sprite *sp = Sprite::create(_fileCapturePath->getCString());
//    sp->setPosition(_winSize.width / 2, _winSize.height / 2);
//    _mainChild->addChild(sp);
    
}
void WingameLayer::runAni() {
    _mainChild->runAction(FadeIn::create(0.3));
    _mainChild->runAction(Sequence::create(ScaleTo::create(0.3, 1, 1), CallFunc::create([&] {
        Label *lb = Label::createWithSystemFont(__String::createWithFormat("Score: %d", _score)->getCString(), "Roboto-Light", 44);
        lb->setAnchorPoint(Point::ANCHOR_MIDDLE_RIGHT);
        lb->setPosition(_winSize.width *9.5 / 18, _winSize.height * 2.1 /4);
        lb->setColor(Color3B::BLACK);
        lb->setOpacity(0);
        lb->setScale(0.5);
        _mainChild->addChild(lb,0,1);
        lb->runAction(EaseBackOut::create(ScaleTo::create(0.3, 1)));
        lb->runAction(Sequence::create(DelayTime::create(0.3),FadeIn::create(0.3), CallFunc::create([&] {
            _lbAniScore = Label::createWithSystemFont(__String::createWithFormat(" + %d", _startAddScore)->getCString(), "Roboto-Light", 44);
            _lbAniScore->setAnchorPoint(Point::ANCHOR_MIDDLE_LEFT);
            _lbAniScore->setPosition(_winSize.width * 9.5 / 18, _winSize.height * 2.1 /4);
            _lbAniScore->setColor(Color3B::BLACK);
            this->addChild(_lbAniScore);
            this->schedule(schedule_selector(WingameLayer::aniUpscore));
        }), NULL));
    }), NULL));
}
void WingameLayer::nextLevel(cocos2d::Ref* pSender) {
    if (!_canGo) return;
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    if (_numRow <= _numCol) {
        this->getParent()->removeAllChildrenWithCleanup(true);
        Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(_numRow + 1, _numCol + 1)));
    } else {
        this->getParent()->removeAllChildrenWithCleanup(true);
        Director::getInstance()->replaceScene(TransitionFade::create(0.6, GameScene::create(_numRow + 1, _numCol + 1)));
    }
}
void WingameLayer::aniUpscore(float dt) {
    _timeAni-= dt;
    if (_timeAni > 0) {
        _startAddScore = _endAddScore * (2 - _timeAni)/ 2;
        _lbAniScore->setString(__String::createWithFormat(" + %d", _startAddScore)->getCString());
    } else {
        _lbAniScore->setString(__String::createWithFormat(" + %d", _endAddScore)->getCString());
        this->unschedule(schedule_selector(WingameLayer::aniUpscore));
        _score += _endAddScore;
        int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
        if (level == 1) {
            UserDefault::getInstance()->setIntegerForKey(scoreEasy, _score);
        } else if (level == 2) {
           UserDefault::getInstance()->setIntegerForKey(scoreNormal, _score);
        } else if (level == 3) {
            UserDefault::getInstance()->setIntegerForKey(scoreHard, _score);
        } else {
            UserDefault::getInstance()->setIntegerForKey(scoreInsane, _score);
        }
        this->checkFinishLevel();
        //tinh luong santa hat nhan duoc khi qua 1 level
        int santaHatdAdd = (int) ceil(_endAddScore / 100.0f);
        this->schedule(schedule_selector(WingameLayer::aniUpSantaHat), 0.23);
        _currentSantaHat = UserDefault::getInstance()->getIntegerForKey(santaHat, 0);
        _totalSantaHat = _currentSantaHat + santaHatdAdd;
        UserDefault::getInstance()->setIntegerForKey(santaHat, _totalSantaHat);
        UserDefault::getInstance()->flush();
    }
}
void WingameLayer::aniUpSantaHat(float dt) {
    Label *lb = static_cast<Label*>(_mainChild->getChildByTag(99));
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("uphat.m4a");
    lb->runAction(Sequence::create(ScaleTo::create(0.1, 1.5),DelayTime::create(0.025), CallFunc::create([&] {
        Label *lb = static_cast<Label*>(_mainChild->getChildByTag(99));
        _currentSantaHat ++;
        lb->setString(__String::createWithFormat("%d", _currentSantaHat)->getCString());
        lb->runAction(ScaleTo::create(0.1, 1));
        if (_currentSantaHat >= _totalSantaHat) {
            this->unschedule(schedule_selector(WingameLayer::aniUpSantaHat));
            this->checkFinishLevel();
        }
    }), NULL));
}
void WingameLayer::checkFinishLevel() {
    int level = UserDefault::getInstance()->getIntegerForKey(selectLevel, 1);
    bool isComplete = false;
    __String *str;
    CallBridge::callShareScoreToGameCenter(_score, level);
    if (level == 1) {
        //easy mo dc normal
        if (_numCol >= 9 && _numRow >= 9) {
            UserDefault::getInstance()->setIntegerForKey(normalFlag, 1);
            if (!UserDefault::getInstance()->getIntegerForKey(medalEasy, 0)) {
                UserDefault::getInstance()->setIntegerForKey(medalEasy, 1);
                isComplete = true;
                str = __String::createWithFormat("You finished EASY Level!");
            }
        }
    } else if (level == 2) {
        //normal mo dc hard
        if (_numCol >= 13 && _numRow >= 13) {
            UserDefault::getInstance()->setIntegerForKey(hardFlag, 1);
            if (!UserDefault::getInstance()->getIntegerForKey(medalNormal, 0)) {
                UserDefault::getInstance()->setIntegerForKey(medalNormal, 1);
                isComplete = true;
                str = __String::createWithFormat("You finished NORMAL Level!");
            }
        }
    } else if (level == 3) {
        //hard mo dc insane
        if (_numCol >= 17 && _numRow >= 17) {
            UserDefault::getInstance()->setIntegerForKey(insaneFlag, 1);
            if (!UserDefault::getInstance()->getIntegerForKey(medalHard, 0)) {
                UserDefault::getInstance()->setIntegerForKey(medalHard, 1);
                isComplete = true;
                str = __String::createWithFormat("You finished HARD Level!");
            }
        }
    } else {
        if (_numCol >= 21 && _numRow >= 21) {
            if (!UserDefault::getInstance()->getIntegerForKey(medalInsane, 0)) {
                UserDefault::getInstance()->setIntegerForKey(medalInsane, 1);
                isComplete = true;
                str = __String::createWithFormat("You finished INSANE Level!");
            }
        }
    }
    if (isComplete) {
        Sprite *sp = Sprite::create("noteLevel.png");
        sp->setPosition(_winSize.width /2 - _winSize.width , _winSize.height /2);
        Label *lb = Label::createWithSystemFont(str->getCString(), "Roboto-Light", 38);
        lb->setColor(Color3B::BLACK);
        lb->setPosition(sp->getContentSize().width / 2, sp->getContentSize().height / 2);
        sp->addChild(lb);
        this->addChild(sp,1,100);
        sp->runAction(Sequence::create(EaseBackOut::create(MoveTo::create(0.4, Point(_winSize.width / 2, _winSize.height / 2))), DelayTime::create(2),EaseBackIn::create(MoveTo::create(0.4, Point(_winSize.width *3 / 2, _winSize.height / 2))), CallFunc::create([&] {
            _canGo = true;
        }), NULL));
    } else {
        _canGo = true;
    }
}
void WingameLayer::setFileCapture(__String *str) {
    CCLOG("%s", str->getCString());
    _fileCapturePath = __String::create(str->getCString());
    _fileCapturePath->retain();
}
void WingameLayer::shareFaceBook(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::shareFacebookPicture(_fileCapturePath->getCString());
}
void WingameLayer::menuLeaderboardCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callShowLeaderboard();
}

void WingameLayer::menuRateCallback(cocos2d::Ref *pSender)
{
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    CallBridge::callAppRate();
}
void WingameLayer::backHome(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("playMenuSound.mp3");
    Director::getInstance()->replaceScene(TransitionFade::create(0.6, IntroScene::createScene()));
}

